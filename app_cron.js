var path = require('path');
global.config = require('./config/config')[(process.env.NODE_ENV || 'development')];

var config = global.config;
require('./config/db')(config);

require(path.join(config.path.conf, 'logger'))(config);

var logger = global.logger,
    cronJob = require('cron').CronJob,
    tasks = require(path.join(config.path.controllers, 'tasks'));

function run() 
{
  function write_results(err, success_msg)
  {
    if (err)  {
      logger.error(err);
    }
    if (success_msg)  {
      logger.info(success_msg);
    }
  }

  new cronJob('0 0 21 * * *', function(){
    tasks.completeChallenge(write_results);
    tasks.addChallenge(write_results);
  }, null, true);

  new cronJob('0 0 22 * * *', function(){
    tasks.updateSitemap(write_results);
  }, null, true);

  new cronJob('0 30 * * * *', function(){
    tasks.findNextChallenge(write_results);
  }, null, true);
}

run();