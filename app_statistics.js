var path = require('path');
global.config = require('./config/config')[(process.env.NODE_ENV || 'development')];

var config = global.config;
require('./config/db')(config);

var async = require('async'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    redis = require('redis'),
    utils = require(path.join(config.path.lib, 'utils'));

var redisSubscribeClient = redis.createClient(),
    redisCache = require(path.join(config.path.lib, 'redis_cache'))
    Challenges = mongoose.model('Challenges'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    Statistics = mongoose.model('Statistics');

redisSubscribeClient.subscribe(config.pubsub.add_word);

redisSubscribeClient.on("message", function (channel, data) 
{
  // console.log(arguments);
  var data = JSON.parse(data);
  async.parallel({
    user: function(callback) {
      redisCache.getUser(data.user, ['login'], callback);
    },
    challenge: function(callback) {
      redisCache.getChallenge(data.challenge, ['word', 'date'], callback);
    }
  }, 
  function(err, res) {
    if (res.user && res.challenge) {
      Statistics.create([{
        user: {
          _id: mongoose.Types.ObjectId(data.user),
          login: res.user.login
        },
        challenge: {
          _id: mongoose.Types.ObjectId(data.challenge),
          word: res.challenge.word,
          date: res.challenge.date
        },
        date: new Date()
      }]);
    }
  });
});