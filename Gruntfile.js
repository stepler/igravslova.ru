module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      production: {
        files: {
          './public/static/js/libs.js': [
            './public/static/js/vendor/moment/moment.js',
            './public/static/js/vendor/moment/locale/ru.js',
            './public/static/js/vendor/lodash.js',
            './public/static/js/vendor/zepto/zepto.js',
            './public/static/js/vendor/zepto/fx.js',
            './public/static/js/vendor/zepto/fx_methods.js',
          ],
          './public/static/js/angular.js': [
            './public/static/js/vendor/angular/angular.js',
            './public/static/js/vendor/angular/angular-animate.js',
            './public/static/js/vendor/angular/angular-ui-router.js',
            './public/static/js/vendor/angular/angular-seo.js',
            './public/static/js/vendor/ui-bootstrap.js',
            './public/static/js/vendor/ui-bootstrap-tpls.js'
          ],
          './public/static/js/app.js': [
            './public/static/app/directives.js', 
            './public/static/app/filters.js', 
            './public/static/app/utils.js', 
            './public/static/app/app.js', 
            './public/static/app/controllers/challenges.js', 
            './public/static/app/controllers/users.js', 
            './public/static/app/controllers/notify.js', 
            './public/static/app/controllers/content.js'
          ]
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      production: {
        files: {
          './public/static/js/libs.min.js': './public/static/js/libs.js',
          './public/static/js/angular.min.js': './public/static/js/angular.js',
          './public/static/js/app.min.js': './public/static/js/app.js',
        }
      }
    },
    less: {
      options: {
        banner: '/*! <%= pkg.name %> v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      development: {
        options: {
          paths: ['./public/static/bem-blocks'],
          yuicompress: true
        },
        files: {
          './public/static/bem-blocks/bem.css': './public/static/bem-blocks/bem.less'
        }
      }
    },
    cssmin: {
      options: {
        banner: '/*! <%= pkg.name %> v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      static_mappings: {
        files: {
          './public/static/css/style.min.css': [
            './public/static/bem-blocks/normilize.css',
            './public/static/bem-blocks/bem.css',
            './public/static/fontello/css/*.css'
          ]
        }
      }
    },
    watch: {
      files: [
        './public/static/bem-blocks/*.less', 
        './public/static/bem-blocks/**/*.less',
        './public/static/js/*.js'
      ],
      tasks: ['less', 'cssmin']
    },
    swig: {
      development: {
        generateSitemap: false,
        generateRobotstxt: false,
        dest: '.',
        src: ['./app/mailer/tpl/source/*.swig'],
      }
    },
    juice: {
      development: {
        files: [{
          expand: true,     // Enable dynamic expansion.
          cwd: './app/mailer/tpl/source/',      // Src matches are relative to this path.
          src: ['*.html'], // Actual pattern(s) to match.
          dest: './app/mailer/tpl/',   // Destination path prefix.
          ext: '.html',   // Dest filepaths will have this extension.
        }]
      }
    },
    clean: {
      css: ['./public/static/bem-blocks/bem.css', './public/static/css/style.css'],
      js: ['./public/static/js/*.js', '!./public/static/js/*.min.js'],
      mailtpl: ['./app/mailer/tpl/source/*.html']
    }
  });
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-juice-email');
  grunt.loadNpmTasks('grunt-swig');

  grunt.registerTask('build_css', ['less', 'cssmin']);
  grunt.registerTask('build_js',   ['concat', 'uglify']);
  grunt.registerTask('build_mailtpl', ['swig', 'juice']);
  grunt.registerTask('default', ['build_css', 'build_js', 'build_mailtpl', 'clean']);
};