$(function() {
  'use strict';

  var $active_word = null,
      $modal = $('#word-info-modal'),
      $modal_title = $('#word-info-modal-title'),
      $modal_btn_apply = $('#word-info-modal-apply'),
      $modal_btn_discard = $('#word-info-modal-discard'),
      $iframes = $('iframe', $modal),

      $userwords_list = $('#userwords-list'),
      $get_info_links = $('.js-userword-info'),
      $words_list = $('.js-word');

  var info_url_tpl = $modal.data('action-url-tpl'),
      action_url_tpl = $userwords_list.data('action-url-tpl');

  $get_info_links.on('click', function() {

    var $link = $(this),
        token = $link.data('token'),
        word = $link.data('word');

    $modal_title.text(word);
    $iframes.each(function() {
      $(this).attr('src', info_url_tpl.replace('_t', token).replace('_r', $(this).data('resource')) );
    });

    $modal.modal('show');
    $active_word = $link;
  });

  $modal.on('click', 'button', function(e) {
    var $btn = $(e.currentTarget);
    if (!$active_word || !$btn.data('action'))
      return;

    set_action($active_word.data('token'), $btn.data('action'), $active_word.parent().next());
  });

  $userwords_list.on('click', 'button', function(e) {
    var $btn = $(e.currentTarget);
    set_action($btn.data('token'), $btn.data('action'), $btn.parent())
  });

  function set_action(token, action, $node)
  {
    $.get(action_url_tpl.replace('_t', token).replace('_a', action))
    .fail(function(){ alert('Ошибка!') })
    .done(function() {
      var res = 
        ( action == 'apply' ?
        '<span class="label label-success">Добавлено</span>' :
        '<span class="label label-danger">Отклонено</span>' ) +
        '<i class="glyphicon glyphicon-send"></i>' ;
      $node.html(res);
    });
  }

});