$(function() {
  'use strict';

  var $form = $('#um-form'),
      $source = $('#um-message-markup'),
      $view = $('#um-message-view'),
      $target = $('#um-message'),
      $global_check_yes = $('#um-is_global'),
      $global_check_no = $global_check_yes.prev().get(0);

  $global_check_yes
  .on('change', function() {
    $global_check_no.disabled = this.checked;
  })
  .trigger('change');

  $source.on('keyup', function() {
    var message =  markdown.toHTML($(this).val());
    $view.html(message);
    $target.val(message);
  });

  $form.on('submit', function() {
    if (!confirm('Отправить?')) {
      return;
    }
    $.ajax({
      url: $form.attr('action') || '',
      type: $form.attr('method') || 'post',
      data: $form.serializeArray()
    })
    .fail(function(){ alert('Ошибка!') })
    .done(function() {
      $form.get(0).reset();
      $global_check_yes.trigger('change');
      $view.html('');
      alert('Готово.')
    });
    return false;
  });
});