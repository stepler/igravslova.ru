# -*- coding: utf-8 -*-
import httplib
import urllib
import re

def load_info(word, resource):

  conn = uri = None

  if resource == 'yandex-spelling':
    uri = urllib.quote('/%s/правописание/' % word.encode('utf-8'))
    conn = httplib.HTTPConnection('slovari.yandex.ru')

  if resource == 'yandex-meaning':
    uri = urllib.quote('/%s/значение/' % word.encode('utf-8'))
    conn = httplib.HTTPConnection('slovari.yandex.ru')

  if resource == 'dic-academic':
    uri = "/searchall.php?SWord=%s&from=ru&to=xx&submitFormSearch=%s&stype=0" % \
      (urllib.quote(word.encode('utf-8')), urllib.quote('Найти'))
    conn = httplib.HTTPConnection('dic.academic.ru')
  
  if not conn:
    return ''

  conn.request('GET', uri)
  response = conn.getresponse()
  data = response.read()
  conn.close()
  
  if response.status != 200:
    return ''

  if resource == 'dic-academic':
    data = re.sub(r'(src|href|link)=\"(?!\/\/)(?!http)(.*?)\"', r'\1="http://dic.academic.ru\2"', data, flags=re.M)

  return data
