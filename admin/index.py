#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
import json
from datetime import datetime

from flask import Flask, request, Response, jsonify, render_template
from functools import wraps
from flask_bootstrap import Bootstrap
from flask.ext.pymongo import PyMongo
import redis

import utils

ENV = os.getenv('NODE_ENV', 'development')
CONFIG = {
  'production': {
    'notify-channel': 'notify',
    'host': 'www.igravslova.ru',
    'port': 5000,
    'db': 'igravslova'
  },
  'pre_production': {
    'notify-channel': 'notify_beta',
    'host': 'beta.igravslova.ru',
    'port': 5001,
    'db': 'igravslova_beta'
  },
  'development': {
    'notify-channel': 'notify_dev',
    'host': 'dev.igravslova.ru',
    'port': 5002,
    'db': 'igravslova_dev'
  }
}

conf = CONFIG[ENV]

def check_auth(username, password):
  """This function is called to check if a username /
  password combination is valid.
  """
  return username == 'admin' and password == 'secret'

def authenticate():
  """Sends a 401 response that enables basic auth"""
  return Response(
  'Could not verify your access level for that URL.\n'
  'You have to login with proper credentials', 401,
  {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    auth = request.authorization
    if not auth or not check_auth(auth.username, auth.password):
      return authenticate()
    return f(*args, **kwargs)
  return decorated


def application(configfile=None):
  app = Flask(__name__)

  app.config['MONGO_DBNAME'] = conf['db']
  app.config['SECRET_KEY'] = '89mBHVPg2hExdKHtqqEG'

  mongo = PyMongo(app)
  Bootstrap(app)

  @app.route('/')
  @requires_auth
  def index():
    return render_template('index.html')


  @app.route('/userword/')
  @requires_auth
  def userword():
    """Список пользовательских слов"""
    
    filter_ = request.args.get('filter')

    if filter_ == 'new':
      words = mongo.db.dictionary_userword.find({'is_apply': False, 'is_discard': False}).sort('date', -1)
    elif filter_ == 'apply':
      words = mongo.db.dictionary_userword.find({'is_apply': True}).sort('date', -1)
    elif filter_ == 'discard':
      words = mongo.db.dictionary_userword.find({'is_discard': True}).sort('date', -1)
    else:
      words = mongo.db.dictionary_userword.find().sort('date', -1)

    return render_template('userword.html', word_list=words, filter=filter_)

  @app.route('/userword/get-info/')
  @requires_auth
  def userword_getinfo():
    """Запрос информации о слове"""

    token = request.args.get('token')
    resource = request.args.get('resource')

    userword = mongo.db.dictionary_userword.find_one({'token': token})
    if not userword:
      return Response('', 404)

    data = utils.load_info(userword['word'], resource)
    return Response(data)

  @app.route('/userword/set-action/')
  @requires_auth
  def userword_setaction():
    """Добавляем или отклоняем слово"""

    token = request.args.get('token')
    action = request.args.get('action')

    if action not in ['apply', 'discard']:
      resp = jsonify()
      resp.status_code = 400
      return resp

    result = mongo.db.dictionary_userword \
      .update({'token': token}, {'$set': {'in_process': action}})
    if result['err']:
      resp = jsonify()
      resp.status_code = 500
      return resp

    return jsonify()

  @app.route('/user_message/', methods=['GET', 'POST'])
  @requires_auth
  def user_message():
    """Отправляем сообщение"""

    if request.method == 'GET':
      return render_template('user_message.html')

    title = request.form['title']
    message = request.form['message']

    # Добавление глобального сообщения
    if 'yes' in request.form['is_global']:
      print(mongo.db.notify_global.insert({
        'title': title,
        'message': message,
        'date': datetime.utcnow()
      }))
      return jsonify()

    # Отправка сообщения сразу пользователям
    # users = mongo.db.users.find()
    # for user in users:
    #   publisher.publish(conf['notify-channel'], \
    #     json.dumps({'user_id': str(user['_id']), 'title': title, 'message': message}))

    return jsonify()

  return app

if __name__ == "__main__":
  application().run(host=conf['host'], port=conf['port'])
