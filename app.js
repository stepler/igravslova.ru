if (module.parent) {
  return;
}

// Load configurations
var path = require('path');

global.config = require('./config/config')[(process.env.NODE_ENV || 'development')];
var config = global.config;

var pjson = require('./package.json');
global.version = pjson.version;

var express = require('express'),
    passport = require('passport');

// Bootstrap logger
require(path.join(config.path.conf, 'logger'))(config);

// Bootstrap db connection
require(path.join(config.path.conf, 'db'))(config);

// Bootstrap language settings
require(path.join(config.path.conf, 'i18n'))(config);

// Bootstrap passport config
require(path.join(config.path.conf, 'passport'))(passport, config);

var app = express();
// express settings
require(path.join(config.path.conf, 'express'))(app, config, passport);

// Bootstrap routes
require(path.join(config.path.conf, 'routes'))(app, passport, config);

var server = require('http').createServer(app);

var notify = notify = require(path.join(config.path.controllers, 'notify'));
require(path.join(config.path.conf, 'socket_io'))(server, notify.sockets(), config);

server.listen(config.app.port);
