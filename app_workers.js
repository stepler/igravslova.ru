var path = require('path');
global.config = require('./config/config')[(process.env.NODE_ENV || 'development')];

var config = global.config;
require('./config/db')(config);

var async = require('async'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    redis = require('redis'),
    utils = require(path.join(config.path.lib, 'utils'));

var tasksCtrl = require(path.join(config.path.controllers, 'tasks')),
    redisCache = require(path.join(config.path.lib, 'redis_cache'));
/*
    Challenges = mongoose.model('Challenges'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers');
*/

utils.init_subscriber(config.pubsub.extend_word, function(channel, data) {
  // tasksCtrl.extendDict(data.user, data.challenge, data.word, function(err, result) {
  //   console.log(arguments);
  //   // TODO уведомление о добавлении
  // });
});

/*
var redisExtendWord = redis.createClient();
redisExtendWord.subscribe(config.pubsub.extend_word);
redisExtendWord.on("message", function(channel, data) {
  var data = JSON.parse(data);
  console.log(data);
  tasksCtrl.extendDict(data.user, data.challenge, data.word, function(err, result) {
    console.log(arguments);
    // TODO уведомление о добавлении
  });
});
*/
/*
var redisSubscribeClient = redis.createClient();
redisSubscribeClient.subscribe(config.pubsub.add_word);
redisSubscribeClient.on("message", function (channel, data) 
{
  var data = JSON.parse(data);

  redisCache.getUserRating(data.user, ['words_count', 'words_list'], 
    function(err, list) {
      // console.log(list);
      if (!~list.words_list.indexOf(data.word)) {
        UsersRating.findOneAndUpdate(
          { user:data.user }, 
          { $push:{ words_list:data.word }, $inc:{ words_count:1 } }, 
          function(err, user) {
            if (!err) {
              redisCache.setUserRating(data.user, ['words_count', 'words_list'], user);
              redisPublishClient.publish(
                config.pubsub.notify, 
                JSON.stringify({ user:data.user, type:'congrads', message:'Новое слово в словаре!' })
              );
            }
          }
        );
      }
    }
  );
});
*/