var validator = require('validator');

validator.extend('notEmpty', function (str) {
  return validator.isLength(str, 1);
});

module.exports = function () {

  return function(req, res, next) {

    var haveErrors = false,
        errors_list = {};

    var validate = function(param, error, validateFunc, source) {
      if (!validator[validateFunc]) {
        throw new Exception('Invalid validator: '+validateFunc);
      }
      if (typeof param === 'string') {
        param = [ param ]
      }
      if (errors_list[param[0]]) {
        return;
      }
      values = [];
      for (var i in param) {
        values.push(source && source[param[i]]);
      }
      if (!validator[validateFunc].apply(validator, values)) {
        haveErrors = true;
        errors_list[param[0]] = {
          path: param[0],
          message: error,
          value: values[0]
        }
      }
    };

    req.validateBody = function(param, error, validateFunc) {
      var args = Array.prototype.slice.call(arguments, 0);
      args.push(req.body);
      validate.apply(this, args);
    };

    req.validateQuery = function(param, error, validateFunc) {
      var args = Array.prototype.slice.call(arguments, 0);
      args.push(req.query);
      validate.apply(this, args);
    };

    req.validationErrors = function() {
      if (!haveErrors) {
        return null;
      }
      return {
        message: 'Validation failed',
        name: 'ValidationError',
        errors: errors_list
      }
    }

    return next();
  }
}