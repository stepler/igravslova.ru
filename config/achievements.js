var config = global.config;

var _ = require('lodash');

var achievements = function(ckeckers) {
  this.list = [];
  this.checkers = {};
  for (var key in ckeckers) {
    this.list.push(key);
    this.checkers[key] = ckeckers[key];
  }
};

achievements.prototype = {
  list: [],
  checkers: {},

  get: function() 
  {
    var args = Array.prototype.slice.call(arguments),
        callback = args.splice(-1,1).pop(),
        user_achievements = args.splice(-1,1).pop(),
        added_achievements = [];

    for (var i in this.list) {
      var item = this.list[i];
      if (~user_achievements.indexOf(item)) {
        continue;
      }
      var result = this.checkers[item].apply(this, args);
      if (result === true) {
        added_achievements.push(item);
      }
    }
    callback(added_achievements, user_achievements.concat(added_achievements));
  }
};

exports.words = new achievements({
  w1: function(words_count) {
    return parseInt(words_count) >= 1;
  },
  w100: function(words_count) {
    return parseInt(words_count) >= 100;
  },
  w500: function(words_count) {
    return parseInt(words_count) >= 500;
  },
  w1000: function(words_count) {
    return parseInt(words_count) >= 1000;
  }
})