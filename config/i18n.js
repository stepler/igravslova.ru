var path = require('path'),
    i18n = require('i18n');

module.exports = function (config) {
  i18n.configure({
    locales: ['ru'],
    defaultLocale: 'ru',
    directory: path.join(config.root, 'locale')

    // // whether to write new locale information to disk - defaults to true
    // updateFiles: false,

    // // what to use as the indentation unit - defaults to "\t"
    // indent: "\t",

    // // setting extension of json files - defaults to '.json' (you might want to set this to '.js' according to webtranslateit)
    // extension: '.js',
  });
}