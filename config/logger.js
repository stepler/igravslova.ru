var path = require('path');

var log4js = require('log4js');

module.exports = function(config) {
  var logger;
  
  log4js.configure({
    appenders: [
      { type: 'console' },
      { type: 'file', filename: config.logger.app }
    ]
  });

  log4js.loadAppender(config.logger.type);
  logger = log4js.getLogger();
  logger.setLevel(config.logger.level);

  global.logger = logger;
};