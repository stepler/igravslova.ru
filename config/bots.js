var config = global.config;

module.exports = [
  // Слабо - часто - день
  {
    user: {
      login: 'zvezdochka777',
      email: 'zvezdochka777@igravslova.ru'
    },
    propability: { play_today: 60, play_new_game: 30 },
    answers: { max: 15, min: 10, start_time: [12,16] }
  },

  // Слабо - часто - день
  {
    user: {
      login: 'Antosha Ermolov',
      email: 'antosha@igravslova.ru'
    },
    propability: { play_today: 65, play_new_game: 20 },
    answers: { max: 10, min: 5, start_time: [14,16] }
  },

  // Слабо - часто - ночь
  {
    user: {
      login: 'КЕКС',
      email: 'keks@igravslova.ru'
    },
    propability: { play_today: 75, play_new_game: 20 },
    answers: { max: 10, min: 7, start_time: [0,2] }
  },

  // Средне - средне - вечер
  {
    user: {
      login: 'Анна Дроздова',
      email: 'annadrozdova@igravslova.ru'
    },
    propability: { play_today: 60, play_new_game: 40 },
    answers: { max: 20, min: 12, start_time: [19,21] }
  },

  // Средне - средне - утро
  {
    user: {
      login: 'petrovich',
      email: 'petrovich@igravslova.ru'
    },
    propability: { play_today: 55, play_new_game: 40 },
    answers: { max: 20, min: 12, start_time: [10,12] }
  },

  // Средне - часто - вечер
  {
    user: {
      login: 'Юлечка',
      email: 'yulichka@igravslova.ru'
    },
    propability: { play_today: 65, play_new_game: 50 },
    answers: { max: 20, min: 10, start_time: [18,20] }
  },

  // Средне - редко - вечер
  {
    user: {
      login: 'Женя Коваев',
      email: 'zhenyakovalev@igravslova.ru'
    },
    propability: { play_today: 30, play_new_game: 100 },
    answers: { max: 15, min: 15, start_time: [12,18] }
  },

  // Сильно - редко - вечер
  {
    user: {
      login: 'Людмила Елисеева',
      email: 'ludmilaeliseeva@igravslova.ru'
    },
    propability: { play_today: 20, play_new_game: 100 },
    answers: { max: 30, min: 30, start_time: [16,19] }
  },

  // Сильно - редко - вечер
  {
    user: {
      login: 'Ekaterina Kameneva',
      email: 'ekaterinakameneva@igravslova.ru'
    },
    propability: { play_today: 30, play_new_game: 40 },
    answers: { max: 32, min: 20, start_time: [12,16] }
  },

  // Сильно - редко - вечер
  {
    user: {
      login: 'Pumputun',
      email: 'pumputun@igravslova.ru'
    },
    propability: { play_today: 40, play_new_game: 30 },
    answers: { max: 35, min: 15, start_time: [20,23] }
  }
/*
  {
    user: {
      login: 'mikki-mouse',
      email: 'mikkimouse@igravslova.ru'
    },
    propability: { play_today: 100, play_new_game: 0 },
    answers: { max: 35, min: 15, start_time: [0,24] }
  },
  */
];