var fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose');

Date.prototype.toString = function(){ return (this.getTime()).toString() };
Date.prototype.toJSON = function(){ return (this.getTime()).toString() };

module.exports = function (config, callback) {

  var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } }
    mongoose.connect(config.db, options)
  }
  mongoose.connection.on('error', function (err) {
    // console.log(err);
  });
  mongoose.connection.on('disconnected', function () {
    connect();
  });
  connect();

  fs.readdirSync(config.path.models).forEach(function (file) {
    if (~file.indexOf('.js')) {
      require(path.join(config.path.models, file));
    }
  });
};