var config = global.config,
    path = require('path');

var LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    YandexStrategy = require('passport-yandex').Strategy,
    VkontakteStrategy = require('passport-vkontakte').Strategy,
    GoogleStrategy = require('passport-google').Strategy,
    user_module = require(path.join(config.path.modules, 'user'))
    utils = require(path.join(config.path.lib, 'utils'));

module.exports = function(passport, config) {

  // serialize sessions
  passport.serializeUser(function(user, done) {
    done(null, user._id)
  });

  passport.deserializeUser(function(id, done) {
    user_module.get_user_session(id, done);
  });

  // use yandex strategy
  passport.use(new YandexStrategy({
      clientID: config.oauth.yandex.clientID,
      clientSecret: config.oauth.yandex.clientSecret,
      callbackURL: 'http://'+config.oauth.host+'/users/login/yandex-callback/'
    },
    strategyVerify('yandex')
  ));

  // use google strategy
  passport.use(new GoogleStrategy({
      realm: config.oauth.google.realm,
      returnURL: 'http://'+config.oauth.host+'/users/login/google-callback/'
    },
    strategyVerify('google')
  ));

  // use facebook strategy
  passport.use(new FacebookStrategy({
      clientID: config.oauth.facebook.clientID,
      clientSecret: config.oauth.facebook.clientSecret,
      callbackURL: 'http://'+config.oauth.host+'/users/login/facebook-callback/'
    },
    strategyVerify('facebook')
  ));

  // use vkontakte strategy
  passport.use(new VkontakteStrategy({
      clientID: config.oauth.vkontakte.clientID,
      clientSecret: config.oauth.vkontakte.clientSecret,
      callbackURL: 'http://'+config.oauth.host+'/users/login/vkontakte-callback/'
    },
    strategyVerify('vkontakte')
  ));

  function strategyVerify(strategy) {
    return function(accessToken, refreshToken, profile, done) 
    {
      user_module.register(
        profile.displayName || profile.username, 
        profile.emails && profile.emails[0] && profile.emails[0].value || null, 
        null, 
        strategy, 
        profile._json, 
        function(err, user) {
          if (err) {
            return done(err);
          }
          return done(err, user);
        }
      );
    }
  }
}
