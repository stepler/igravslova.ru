var config = global.config,
    logger = global.logger,
    path = require('path');

var express = require('express'),
    swig = require('swig'),
    app_validator = require('../middlewares/app_validator'),
    mongoStore = require('connect-mongo')(express);

var env = process.env.NODE_ENV || 'development';

module.exports = function (app, config, passport) {

  // set views path, template engine and default layout
  app.set('views', config.path.views);
  app.engine('swig', swig.renderFile);
  // app.set('view engine', 'html');
  app.set('view engine', 'swig');
  app.set('view cache', false);

  app.use(express.favicon(path.join(config.path.public, 'favicon.ico')));
  app.use(express.static(config.path.public));

  // development env config
  app.configure('development', function(){
    app.locals.pretty = true;
    app.use(express.logger('dev'));
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  });

  app.configure(function () {

    // cookieParser should be above session
    app.use(express.cookieParser());

    // bodyParser should be above methodOverride
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(express.methodOverride());
    app.use(app_validator())
    
    // express/mongo session storage
    app.use(express.session({
      key: config.session.key,
      secret: config.session.secret,
      store: new mongoStore({
        url: config.db,
        collection : config.session.storage
      })
    }));

    // use passport session
    app.use(passport.initialize());
    app.use(passport.session());

    // connect flash for flash messages - should be declared after sessions
    // app.use(flash())

    app.use(express.csrf());
    app.use(function(req, res, next){
      res.locals.version = global.version;
      res.locals.req = req;
      res.locals.env = env;
      res.locals.user = req.user;
      res.locals.conf = config;
      res.locals.csrf_token = req.csrfToken();
      next();
    });

    // routes should be at the last
    app.use(app.router);

    // assume "not found" in the error msgs
    // is a 404. this is somewhat silly, but
    // valid, you can do whatever you like, set
    // properties, use instanceof etc.
    app.use(function(err, req, res, next){
      // treat as 404
      if (err.message
        && (~err.message.indexOf('not found')
        || (~err.message.indexOf('Cast to ObjectId failed')))) {
        return next();
      }

      // log it
      // send emails if you want
      // console.error(err.stack);

      // error page
      logger.error(err);
      if (req.xhr) 
        return res.json(500, {});

      res.status(500).render('500', { error: err.stack });
    });

    // assume 404 since no middleware responded
    app.use(function(req, res, next){
      res.status(404).render('404', {
        url: req.originalUrl,
        error: 'Not found'
      })
    });
  });
}
