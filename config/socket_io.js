var express = require('express'),
    mongoStore = require('connect-mongo')(express),
    socket_io = require('socket.io');

module.exports = function(server, controller, config) {

  var sessionStore = new mongoStore({ 
        url: config.db,
        collection : config.session.storage
      }),
      parseCookie = express.cookieParser(config.session.secret),
      io = socket_io.listen(server);

  // config
  io.enable('browser client minification');
  io.enable('browser client etag');
  io.enable('browser client gzip');
  io.set('log level', 0);
  io.set('transports', ['websocket','flashsocket','htmlfile','xhr-polling','jsonp-polling']);
  
  io.set('authorization', function(handshake, accept) {
    parseCookie(handshake, null, function(err) {
      if (err) {
        return accept(null, false);
      }
      var sessionID = handshake.signedCookies[config.session.key];
      sessionStore.get(sessionID, function(err, session) {
        if (err || !(session.passport && session.passport.user)) {
          return accept(null, false);
        }
        handshake.user_id = session.passport.user;
        accept(null, true);
      });
    });
  });

  io.sockets.on('connection', function(socket) {
    if (!socket.handshake.user_id) {
      return;
    }
    controller.connection(socket, socket.handshake.user_id);

    socket.on('disconnect', function () {
      controller.disconnect(socket, socket.handshake.user_id);
    });
  });
};