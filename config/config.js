var path = require('path');

var _ = require('lodash'),
    rootPath = path.normalize(__dirname + '/..')

var source_conf = {
  version: '2.1.1',
  db: 'mongodb://127.0.0.1:27017/igravslova',
  root: rootPath,
  path: {
    conf: path.join(rootPath, 'config'),
    public: path.join(rootPath, 'public'),
    controllers: path.join(rootPath, 'app', 'controllers'),
    models: path.join(rootPath, 'app', 'models'),
    modules: path.join(rootPath, 'app', 'modules'),
    pubsub: path.join(rootPath, 'app', 'pubsub'),
    views: path.join(rootPath, 'app', 'views'),
    tests: path.join(rootPath, 'tests'),
    lib: path.join(rootPath, 'lib'),
    middlewares: path.join(rootPath, 'middlewares'),
    mail: path.join(rootPath, 'app', 'mailer'),
    mail_tpl: path.join(rootPath, 'app', 'mailer', 'tpl'),
  },
  logger: {
    type: 'file',
    level: 'INFO',
    app: path.join(path.dirname(rootPath), 'logs', 'app.log')
  },
  app: {
    host: 'www.igravslova.ru',
    httphost: 'http://www.igravslova.ru',
    port: 8080,
    name: 'Igra v slova',
    seo_port: 9090
  },
  session: {
    key: 'sid',
    secret: 'Igra v slova',
    storage: 'sessions'
  },
  mail: {
    smtp: {
      provider: 'yandex',
      user: 'monkey@igravslova.ru',
      pass: 'm0nk3y'
    },
    fromEmail: 'monkey@igravslova.ru',
    adminEmail: 'simon.moiseenko@gmail.com'
  },
  oauth: {
    host: 'www.igravslova.ru',
    yandex: {
      clientID: '0448f61c91184b46b088913e0de2f484',
      clientSecret: 'dd9b297b6dc146efb6df0065fbaffcd3'
    },
    google: {
      realm: 'http://www.igravslova.ru'
    },
    facebook: {
      clientID: '215056118695214',
      clientSecret: '001c1a066843e1dbe800228bf9d0a8b1'
    },
    vkontakte: {
      clientID: '4186335',
      clientSecret: 'uwxiaiJFMXdTN9c5lZ8X'
    }
  },
  pubsub: {
    notify: 'notify',
    add_word: 'add_word',
    extend_word: 'extend_word'
  },
  challenges: {
    awards: ['', 'g', 's', 'b'],
    score: [0, 180, 60, 20],
    words_length: [12, 13, 14, 15, 16],
    active_days: 7,
    active_time_from: { h:0, m:0, s:0, ms:0 },
    active_time_to: { h:12, m:0, s:0, ms:0 },
    first_challenge: 1393372800000
  },
  notify: {
    cookie_name: 'notify_ts',
    active_days: 30
  }
};

var conf = {
  production: {},
  pre_production: {},
  development: {},
  tests: {}
};

_.merge(conf.production, source_conf, {});
_.merge(conf.pre_production, source_conf, {
  db: 'mongodb://127.0.0.1:27017/igravslova_beta',
  app: { host: 'beta.igravslova.ru', httphost: 'http://beta.igravslova.ru', port: 8081 },
  pubsub: {
    notify: 'notify_beta',
    add_word: 'add_word_beta',
    extend_word: 'extend_word_beta'
  },
  oauth: { 
    host: 'beta.igravslova.ru',
    yandex: {
      clientID: '1a2833a6023847ebb529c398bdef4813',
      clientSecret: '4ecd627f4bce4912bb2b2ddca79caa9e'
  }},
  path: {
    mail: path.join(rootPath, 'tests', 'override'),
  }
});
_.merge(conf.development, source_conf, {
  db: 'mongodb://127.0.0.1:27017/igravslova_dev',
  app: { host: 'dev.igravslova.ru', httphost: 'http://dev.igravslova.ru', port: 8888 },
  logger: { type: 'console', level: 'TRACE' },
  pubsub: {
    notify: 'notify_dev',
    add_word: 'add_word_dev',
    extend_word: 'extend_word_dev'
  },
  oauth: { 
    host: 'dev.igravslova.ru',
    yandex: {
      clientID: '176578a298e84d12b00e88eaed7b2a3e',
      clientSecret: '30400aa389f542269d5b267884027642'
  }},
  path: {
    mail: path.join(rootPath, 'tests', 'override'),
  }
});
_.merge(conf.tests, source_conf, {
  db: 'mongodb://127.0.0.1:27017/igravslova_test',
  path: {
    pubsub: path.join(rootPath, 'tests', 'override'),
    mail: path.join(rootPath, 'tests', 'override'),
  },
  challenges: { words_length: [9, 9] }
});

module.exports = conf;