var path = require('path');

module.exports = function (app, passport, config) {

  // challenges routes
  var challenges = require(path.join(config.path.controllers, 'challenges'));
  app.get( '/api/challenges', challenges.list);
  app.get( '/api/challenges/:date', challenges.get);
  app.put( '/api/challenges/:date', challenges.add_answer);
  app.post('/api/challenges/:date/extend', challenges.extend);
  app.get( '/api/challenges/answers/:user_id', challenges.list_answers);
  // app.post('/api/challenges/:date/', challenges.update);

  // rating routes
  var rating = require(path.join(config.path.controllers, 'rating'));
  app.get('/api/rating', rating.list);
  app.get('/api/rating/:user_id', rating.get);

  // users routes
  var users = require(path.join(config.path.controllers, 'users'));
  app.post('/api/users', users.register);
  app.post('/api/users/request-email',  users.emailRequest);
  app.put( '/api/users/restore',  users.restoreRequest);
  app.post('/api/users/restore',  users.restore);
  app.put( '/api/users/:user_id',  users.update);
  app.post('/users/login', users.login);
  app.get( '/users/confirm-email', users.emailConfirm);

  // oAuth routes
  app.get('/users/login/yandex', passport.authenticate('yandex'));
  app.get('/users/login/yandex-callback/', users.authenticate('yandex'));
  app.get('/users/login/google', passport.authenticate('google'));
  app.get('/users/login/google-callback/', users.authenticate('google'));
  app.get('/users/login/facebook', passport.authenticate('facebook', { scope:'email' }));
  app.get('/users/login/facebook-callback/', users.authenticate('facebook'));
  app.get('/users/login/vkontakte', passport.authenticate('vkontakte'));
  app.get('/users/login/vkontakte-callback/', users.authenticate('vkontakte'));

  app.get('/users/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

  // main and content route
  var main = require(path.join(config.path.controllers, 'main')),
      notify = require(path.join(config.path.controllers, 'notify'));
  app.get('/content/rules', main.content);
  app.post('/feedback', main.feedback);
  app.get('/notify/global-messages', notify.global_messages);

  app.get('/*', main.index)
}