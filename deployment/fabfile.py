#! coding:utf-8
import os
import uuid

from fabric.api import *
from fabtools import nodejs, files, python

NODE_MODULES_DIR = "node_modules"
PYTHON_VENV_DIR = "python_env"

env.roledefs["root"] = ["simon@igravslova.ru"]
env.roledefs["production"] = ["igravslova_ru@igravslova.ru"]
env.roledefs["beta"] = ["beta_igravslova_ru@igravslova.ru"]
env.roledefs["dev"] = ["dev_igravslova_ru@igravslova.ru"]

@task
@roles("production")
def deploy(check_env=False):
    make_deploy(check_env, "igravslova", "master", "/home/www/igravslova.ru/docs")

@task
@roles("beta")
def deploy_beta(check_env=False, update_db=False):
    make_deploy(check_env, "igravslova_beta", "beta", "/home/www/beta.igravslova.ru/docs")
    if update_db:
        run("mongo igravslova_beta --eval \"printjson(db.dropDatabase());printjson(db.copyDatabase('igravslova','igravslova_beta'))\"")

@task
@roles("dev")
def deploy_dev(check_env=False, update_db=False):
    make_deploy(check_env, "igravslova_dev", "HEAD", "/home/www/dev.igravslova.ru/docs")
    if update_db:
        run("mongo igravslova_dev --eval \"printjson(db.dropDatabase());printjson(db.copyDatabase('igravslova','igravslova_dev'))\"")


def make_deploy(check_env, svgroup, branch, project_root):
    tmp_file = "/tmp/%s.tar.gz" % uuid.uuid4().hex
    venv_dir = os.path.join(project_root, PYTHON_VENV_DIR)

    local("git archive --format=tar.gz -o {output} -9 {branch}".format(
          output=tmp_file,
          branch=branch))
    put(tmp_file, tmp_file)

    run("find {project_root} -maxdepth 1 -mindepth 1 | grep -v -E '{node_modules}|{python_env}' | xargs rm -rf".\
        format(project_root=project_root, node_modules=NODE_MODULES_DIR, python_env=PYTHON_VENV_DIR))

    run("tar -xzf {filename} -C {project_root}".\
        format(project_root=project_root, filename=tmp_file))
    run("rm -f {filename}".format(filename=tmp_file))
    local("rm -f {filename}".format(filename=tmp_file))

    with cd(project_root):
        if check_env:
            if not files.is_dir(venv_dir):
                run("virtualenv {venv}".format(venv=PYTHON_VENV_DIR))
            with python.virtualenv(venv_dir):
                python.install_requirements("python_requirements.txt")
            nodejs.install_dependencies()

        run("supervisorctl -s unix:///var/run/supervisor.sock restart {group}:".format(group=svgroup))

@task
@roles("root")
def switch_hosts(prod_to_old=False):
    prod_conf = "/home/www/igravslova.ru/conf/nginx.conf"
    old_conf = "/home/www/old.igravslova.ru/conf/nginx.conf"
    if prod_to_old:
        sudo("sed -i 's/server_name www.igravslova.ru/server_name old.igravslova.ru/' %s" % prod_conf)
        sudo("sed -i 's/server_name old.igravslova.ru/server_name www.igravslova.ru/' %s" % old_conf)
    else:
        sudo("sed -i 's/server_name old.igravslova.ru/server_name www.igravslova.ru/' %s" % prod_conf)
        sudo("sed -i 's/server_name www.igravslova.ru/server_name old.igravslova.ru/' %s" % old_conf)
    sudo("service nginx reload")

