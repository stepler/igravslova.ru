var _ = require('lodash'),
    crypto = require('crypto'),
    i18n = require('i18n');

exports.setRatingsPosition = function(list, position, ratingVar) 
{
  var lastRating = (list[0] && list[0][ratingVar]) || 0;

  list.forEach(function(item, key) {
    if (item[ratingVar] != lastRating) {
      position += 1;
    }
    lastRating = item[ratingVar];
    list[key]['position'] = position;
  });
  return list;
}

exports.parseErrors = function(err, code, field) 
{
  var field_aliases = {
    password_hash: 'password'
  };

  if (_.isObject(err) && err.name == 'ValidationError') 
  {
    var errors_list = [];
    for (field in err.errors) {
      var field = field_aliases[field] || field,
          code  = err.errors[field].message;
      errors_list.push({ 
        field: field, 
        code: code,
        message: i18n.__(field+'__'+code)
      });
    }
    return { status:400, message:errors_list };
  }

  if (_.isString(field) && _.isString(code)) {
    return { status:400, message:[{ 
      field: field_aliases[field] || field, 
      code: code, 
      message: i18n.__(field+'__'+code) 
    }] };
  }

  return { status:500, message:[{ 
    field: '', 
    code: 'unknown_error', 
    message: i18n.__('unknown_error') 
  }] } ;
}

exports.ValidationError = function(field, code) {
  return new (function(field, code){
    this.name = 'ValidationError';
    this.errors = {};
    this.errors[field] = { path:field, message:code, value:'' }
  })(field, code);
}

exports.genToken = function()
{
  var salt = Math.round((new Date().valueOf() * Math.random())) + '',
      param = Math.round((new Date().valueOf() * Math.random())) + '';
  return crypto.createHmac('sha1', salt).update(param).digest('hex');
}

exports.get_pagination = function(pager, count)
{
  return {
    page: pager.page,
    pages: Math.ceil(count / pager.limit),
    items: count,
    limit: pager.limit
  };
}