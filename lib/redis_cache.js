var config = global.config,
    path = require('path');

var _ = require('lodash'),
    redis = require('redis'),
    mongoose = require('mongoose');

var redisClient = redis.createClient(),
    Challenges = mongoose.model('Challenges'),
    Users = mongoose.model('Users'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    UsersRating = mongoose.model('UsersRating'),
    Dictionary = mongoose.model('Dictionary');

module.exports = {
  getBotPlayTime: function(user_id, field, callback)
  {
    var model = {}
    if (!_.isArray(field)) { field = [field]; }
    field.forEach(function(key){ model[key] = 0; })
    
    this.get_data(
      function(cb){ cb(null, model) },
      'bot', user_id, field, callback
    );
  },

  setBotPlayTime: function(user_id, fields, model, callback) 
  {
    var args = Array.prototype.slice.call(arguments, 0);
    args.unshift('bot');
    this.set_data.apply(this, args);
  },

  getUserRating: function(user_id, field, callback)
  {
    this.get_data(
      function(cb){ UsersRating.findOne({ user:user_id }, cb) },
      'rating', user_id, field, callback
    );
  },

  setUserRating: function(user_id, fields, model, callback) 
  {
    var args = Array.prototype.slice.call(arguments, 0);
    args.unshift('rating');
    this.set_data.apply(this, args);
  },


  getUser: function(user_id, field, callback) 
  {
    this.get_data(
      function(cb){ Users.findById(user_id, cb) },
      'user', user_id, field, callback
    );
  },

  setUser: function(user_id, fields, model, callback) 
  {
    var args = Array.prototype.slice.call(arguments, 0);
    args.unshift('user');
    this.set_data.apply(this, args);
  },

  getChallenge: function(challenge_id, field, callback) 
  {
    this.get_data(
      function(cb){ Challenges.findById(challenge_id, cb) },
      'challenge', challenge_id, field, callback
    );
  },

  setChallenge: function(challenge_id, fields, model, callback) 
  {
    var args = Array.prototype.slice.call(arguments, 0);
    args.unshift('challenge');
    this.set_data.apply(this, args);
  },

  get_data: function(get_model, key_prefix, challenge_id, field, callback) 
  {
    var model = null,
        key = this._get_key(challenge_id, key_prefix);
    
    this._get_data(key, field, callback,
      function(field, callback) {
        var self = this;
        if (_.isNull(model)) {
          get_model(function(err, _model) {
            if (err) {
              callback.call(self, err, null);
              return;
            }
            model = _model;
            self._set_data(key, field, model[field], callback);
          });
        }
        else {
          self._set_data(key, field, model[field], callback);
        }
      }
    );
  },

  set_data: function(key_prefix, challenge_id, fields, model, callback) {
    var key = this._get_key(challenge_id, key_prefix);
    this._save_data(model, key, fields, callback);
  },

  _get_data: function(key, fields, callback, update) 
  {
    var self = this,
        results = {},
        field = null,
        in_process = 0;

    if (!_.isArray(fields)) {
      fields = [fields];
    }

    var cb = function(err, reply, field_from_update) {
      if (err) {
        callback.call(self, err);
        return;
      }
      if (!_.isNull(field)) 
      {
        if (field_from_update || !_.isNull(reply)) {
          results[(field_from_update || field)] = JSON.parse(reply);
          in_process -= 1;
        }
        else {
          update.call(self, field, cb);
        }
      }
      if (fields.length) {
        field = fields.pop();
        in_process += 1;
        self._redis_get(key, field, cb);
      }
      else {
        if (in_process === 0) {
          callback.call(self, null, results);
        }
      }      
    };

    cb();
  },

  _save_data: function(model, key, fields, callback) 
  {
    var self = this,
        in_process = 0,
        callback = callback || function(){};

    if (!_.isArray(fields)) {
      fields = [fields];
    }

    var cb = function(err, res) {
      if (err) {
        callback.call(self, err);
        return;
      }
      if (res) {
        in_process -= 1;
      }
      if (fields.length) {
        var field = fields.pop();
        in_process += 1;
        self._set_data(key, field, model[field], cb);
      }
      else {
        if (in_process === 0) {
          callback.call(self, null);
        }
      }      
    };

    cb();
  },

  _set_data: function(key, field, value, callback) 
  {
    var self = this;
    // if (_.isObject(value)) {
      value = JSON.stringify(value);
    // }
    self._redis_set(key, field, value, function(err) {
      if (err) {
        callback.call(self, err, null);
        return;
      }
      callback.call(self, null, value, field);
    });
  },

  _redis_get: function(key, field, cb)
  {
    // redisClient.hdel(key, field, function(){ cb(null, null); });
    redisClient.hget(key, field, cb);
  },

  _redis_set: function(key, field, value, cb)
  {
    redisClient.hset(key, field, value, cb);
  },

  _get_key: function(id, prefix) {
    return prefix+'_'+id;
  }
};