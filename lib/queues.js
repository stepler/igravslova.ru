var redis = require("redis"),
    client = redis.createClient();

client.on("message", function (channel, message) {
    console.log("client channel " + channel + ": " + message);
});

client.subscribe("stream");