var config = global.config,
    logger = global.logger,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils')),
    redisCache = require(path.join(config.path.lib, 'redis_cache'));

var Challenges = mongoose.model('Challenges'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    Users = mongoose.model('Users');


var player = function(user, propability, answers) 
{
  if (!_.isObject(user)
      || !_.isObject(propability)
      || !_.isObject(answers))
    return false;

  if (!_.has(user, 'login') || !_.has(user, 'email'))
    return false;

  if (!_.isNumber(propability.play_today)
      || !_.isNumber(propability.play_new_game))
    return false;

  if (!_.isNumber(answers.min)
      || !_.isNumber(answers.max)
      || !_.isArray(answers.start_time))
    return false;

  if (_.isObject(answers.timeout)) 
  {
    for (var time in answers.timeout)
    {
      if (!_.isNumber(time) || time > 100)
        return false;
      if (!_.isArray(answers.timeout[time]) 
          || answers.timeout[time].length != 2
          || !_.isNumber(answers.timeout[time][0])
          || !_.isNumber(answers.timeout[time][1]) )
        return false;
    }
  }
  else
    answers.timeout = { 30:[1,8], 60:[5,30], 100:[20,120] };

  if (answers.start_time.length != 2
      || !_.isNumber(answers.start_time[0])
      || !_.isNumber(answers.start_time[1]) )
    return false;

  var self = this;
  this.user = user;
  this.propability = propability;
  this.answers = answers;

  this._get_player(function(err, user_id) {
    if (err) {
      logger.error(err);
      return;
    }

    self.user_id = user_id;
  });

  return this;
};

player.prototype = 
{
  user_id: 0,
  user: {
    login: null,
    email: null,
  },
  propability: {
    play_today: 60, 
    play_new_game: 30
  },
  answers: {
    max: 30,
    min: 15,
    timeout: { 30:[1,8], 60:[5,30], 100:[20,120] },
    start_time: [18,21]
  },

  _get_player: function(callback)
  {
    var self = this;
    Users.findOne({ email:self.user.email }, function(err, user) {
      if (err)
        return callback.apply(self, err);
      
      if (user) 
        return callback(null, user._id);
      
      var user = new Users({
        login: self.user.login,
        email: self.user.email,
        password: utils.genToken(),
        provider: 'bot',
        profile: {}
      });
      user.save(function(err) {
        if (err)
          return callback(err);
        return callback(null, user._id);
      });
    });
  },

  tryToPlay: function()
  {
    if (!this.user_id) {
      logger.error("Undefined user_id");
      return; // TODO: write to log
    }

    var self = this;
    redisCache.getBotPlayTime(this.user_id, ['start_time'], 
      function(err, list) {
        if (err) {
          logger.error("Error on getting redis_cache");
          logger.trace(err);
          return;
        }
        
        var set_time = false;
        if (!list.start_time || list.start_time === 0)
          set_time = true;

        if (list.start_time) 
        {
          if (true || self._is_time_to_play(list.start_time)){
            set_time = true;
            logger.debug("User is going to play: "+self.user_id);
            // GO-Go-GO
            self.play();
          }
        }

        if (set_time) {
          redisCache.setBotPlayTime(self.user_id, ['start_time'], 
            { start_time:self._get_next_play_time() });
          logger.debug("User is set next play time: "+self.user_id);
        }
      }
    );
  },

  play: function()
  {
    if (!this.isPlayToday())
      return;

    if (this.isPlayNewGame())
      this.playNewGame();
    else
      this.findGameToPlay();
  },

  isPlayToday: function()
  {
    return _.random(0, 100) <= this.propability.play_today ;
  },

  isPlayNewGame: function()
  {
    return _.random(0, 100) <= this.propability.play_new_game ;
  },

  playNewGame: function()
  {
    var self = this;

    logger.debug("User play new game: "+self.user_id);

    Challenges.findOne( {}, { date:1 }, { sort:'-date'},
      function(err, challenge) 
      {
        if (err) {
          logger.error("Error on find last challenge");
          logger.trace(err);
          return;
        }

        if (!challenge) {
          logger.error("Unable to gel last challenge");
          return; 
        }

        ChallengesAnswers
        .findOne(
          { user: self.user_id, challenge: challenge._id }, 
          { __v:0, answers_list:0, sort:0 },
          function(err, challenge_answers) {
            if (err)
              return; // TODO: write to log

            if (challenge_answers)
              return ckeck_to_play(challenge_answers);

            var challenge_answers = new ChallengesAnswers({
              challenge: challenge._id,
              user: self.user_id,
              sort: challenge.date.getTime(),
              answers_list: [],
              answers_count: 0
            });
            challenge_answers.save(function(err) {
              if (err) {
                logger.error("Error on save new challenge");
                logger.trace(err);
                return; // TODO: write to log
              }
              ckeck_to_play(challenge_answers)
            });
          }
        );
      }
    );

    function ckeck_to_play(challenge_answers) {
      self._isNeedToPlayGame(challenge_answers, function(err, game_to_play) {
        if (err || !game_to_play) {
          logger.debug("No need play");
          return; // TODO: write to log
        }
        self._playExistGame(game_to_play.challenge_answers, game_to_play.need_to_answer, game_to_play.total_answers);
      });
    }
  },

  findGameToPlay: function()
  {
    var self = this;

    ChallengesAnswers
    .find(
      { user: this.user_id,  }, 
      { __v:0, answers_list:0, sort:0 })
    .sort({'sort': -1})
    .limit(5)
    .exec(function(err, list) 
    {
      if (err)
        return callback.apply(this, [err, null, null]);
      
      var tasks = [];
      list.forEach(function(challenge_answers)
      { 
        tasks.push(function(callback) {
          self._isNeedToPlayGame(challenge_answers, function(err, game_to_play) { callback(game_to_play, err) });
        });
      });

      async.waterfall(tasks, function (game_to_play) {
        if (game_to_play)
          self._playExistGame(game_to_play.challenge_answers, game_to_play.need_to_answer, game_to_play.total_answers);
        else
          self.playNewGame();
      });
    });
  },

  _playExistGame: function(challenge_answers, need_to_answer, total_answers)
  {
    var self = this;
    logger.debug("User play exist game: "+challenge_answers._id);
    var set_answer = function()
    {
      if (challenge_answers.answers_count >= need_to_answer)
        return;

      var timeout = null,
          timeout_range = self._get_answers_timeout(challenge_answers.answers_count, total_answers);
      if (!timeout_range)
        return;
      
      timeout =  _.random(timeout_range[0], timeout_range[1]);
      logger.debug("User answer timeout: "+timeout);
      setTimeout(function() {
        challenge_answers.answers_count += 1;
        challenge_answers.save(function(err) {
          if (err)
            return; // TODO: write to log
          logger.debug("User is set new_answer: "+challenge_answers.user);
          set_answer();
        });
      }, timeout);
    }
    set_answer()
  },

  _isNeedToPlayGame: function(challenge_answers, callback)
  {
    var self = this,
        my_answers = challenge_answers.answers_count;

    this._getGameAnswers(
      challenge_answers.challenge, 
      true, 
      function(err, list, total_answers) 
      {
        var user_answers = self._get_answers_on_position(list, 1)
            need_to_answer = self._needToAnswer(my_answers, user_answers, total_answers);

        if (need_to_answer > 0)
          return callback(null, { challenge_answers:challenge_answers, need_to_answer:need_to_answer, total_answers:total_answers });

        return callback(null, null);
      }
    )
  },

  _getGameAnswers: function(challenge_id, with_total_answers, callback)
  {
    var self = this;
    ChallengesAnswers
    .find(
      { challenge: challenge_id }, 
      { __v:0, challenge:0, answers_list:0, sort:0 })
    .populate('user', { login:1, avatar:1, is_bot:1 })
    .sort({'answers_count': -1})
    .limit(10)
    .exec(function(err, list) 
    {
      if (err)
        return callback.apply(this, [err, null, null]);
      
      if (!with_total_answers)
        return callback.apply(self, [err, list, null]);
      
      Challenges.findById(
        challenge_id, 
        { answers_count: 1 }, 
        function(err, challenge) 
        {
          if (err || !challenge)
            return callback.apply(this, [err, null, null]);
          
          list = utils.setRatingsPosition(list, 1, 'answers_count');
          return callback.apply(self, [err, list, challenge.answers_count ]);
        }
      );
    });
  },

  _needToAnswer: function(my_answers_count, user_answers_count, total_answers_count)
  {
        // max и min кол-во в единицах
    var max_answers_count = this._to_count(this.answers.max, total_answers_count),
        min_answers_count = this._to_count(this.answers.min, total_answers_count),
        // процент ответов пользователя от максимального для бота (this.answers.max)
        user_answers = this._to_percentage(user_answers_count, max_answers_count),
        need_answers_count = 0;

    if (user_answers > 100)
      return 0;

    need_answers_count = 
      Math.ceil(this._to_count(user_answers, (max_answers_count-min_answers_count))
        + min_answers_count);

    return need_answers_count - my_answers_count;
  },

  _to_count: function(current_p, max_c)
  {
    return current_p * max_c / 100;
  },

  _to_percentage: function(current_c, max_c)
  {
    return current_c * 100 / max_c;
  },

  _get_answers_on_position: function(list, position)
  {
    for (var i in list)
    {
      if (list[i].position == position)
        return list[i].answers_count;
    }
    return 0;
  },

  _get_answers_timeout: function(my_answers_count, total_answers_count)
  {
    var max_answers_count = this._to_count(this.answers.max, total_answers_count),
        my_answers_percentage = this._to_percentage(my_answers_count, max_answers_count),
        range = null;
    for (var percentage_range in this.answers.timeout)
    {
      if (percentage_range > my_answers_percentage)
      {
        range = this.answers.timeout[percentage_range];
        if (!_.isArray(range) || range.length != 2)
          return null;
        return [ range[0]*1000, range[1]*1000 ];
      }
    }
    return null;
  },

  _is_time_to_play: function(start_time)
  {
    var start_from = moment(),
        start_to = start_from.clone().add('m', 10);

    start_time = parseInt(start_time);
    start_from = start_from.toDate().getTime();
    start_to = start_to.toDate().getTime();

    var d1 = start_to - start_from,
        d2 = start_to - start_time;

    return (d2 > 0 && d2 < d1);
  },

  _get_next_play_time: function()
  {
    var range_hour = this.answers.start_time;
    if (range_hour.length != 2)
      return 0;

    var now_hours = moment().hours(),
        hours = _.random(parseInt(range_hour[0]), parseInt(range_hour[1])),
        minutes = _.random(0, 59);

    if (minutes%10 === 0)
      minutes += 1;

    // return moment();
    return moment()
      .add('d', ((now_hours-1)<hours ? 1 : 0 ))
      .hours(hours)
      .minutes(minutes)
      .seconds(0)
      .milliseconds(0)
      .toDate()
      .getTime();
  }
};

module.exports = player; 
