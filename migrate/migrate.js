var fs = require('fs'),
    path = require('path');

global.config = require('../config/config')[(process.env.NODE_ENV || 'development')];
var config = global.config;

var orm = require('orm'),
    mongoose = require('mongoose'),
    async = require('async'),
    moment = require('moment'),
    utils = require(path.join(config.path.lib, 'utils'));

var DB = {}
    DB_= null;

var connect = function () {
  var options = { server: { socketOptions: { keepAlive: 1 } } }
  mongoose.connect(config.db, options);
  
  fs.readdirSync(config.path.models).forEach(function (file) {
    if (~file.indexOf('.js')) {
      require(path.join(config.path.models, file));
    }
  });

  var mysql_connect = 'mysql://igravslova_ru:DSmDsjr3BvstLM74@localhost/igravslova_ru';
  orm.express(mysql_connect, {
    define: function (db, models) {
      DB_ = db;
      db.settings.set("properties.primary_key", "id");

      db.load("./old_models", function (err) {
        if (err) {
          console.log(err);
          return;
        }
        DB = models = db.models;
        setTimeout(function(){
          run();
        }, 500);
      });
    }
  })
}
connect();

mongoose.connection.on('error', function (err) {
  console.log(err);
})

function run() {

  var REFS = {
    challenges: {},
    users: {},
  };
  var WORDS = [],
      USED_WORDS = [],
      USERS_WORDS = {};

  var migrate_tasks = [];

  var Challenges = mongoose.model('Challenges'),
      ChallengesAnswers = mongoose.model('ChallengesAnswers'),
      Users = mongoose.model('Users'),
      UsersRating = mongoose.model('UsersRating'),
      Dictionary = mongoose.model('Dictionary'),
      DictionaryUserword = mongoose.model('DictionaryUserword');

  migrate_tasks.push(function(callback) {
    if (!mongoose.connection.collections['challenges'])
      return callback(null);
    mongoose.connection.collections['challenges'].drop( function(err) {
      console.log('Challenges collection dropped');
      callback(null);
    });
  });
  migrate_tasks.push(function(callback) {
    if (!mongoose.connection.collections['users'])
      return callback(null);
    mongoose.connection.collections['users'].drop( function(err) {
      console.log('Users collection dropped');
      callback(null);
    });
  });
  migrate_tasks.push(function(callback) {
    if (!mongoose.connection.collections['challenges_answers'])
      return callback(null);
    mongoose.connection.collections['challenges_answers'].drop( function(err) {
      console.log('ChallengesAnswers collection dropped');
      callback(null);
    });
  });
  migrate_tasks.push(function(callback) {
    if (!mongoose.connection.collections['users_rating'])
      return callback(null);
    mongoose.connection.collections['users_rating'].drop( function(err) {
      console.log('UsersRating collection dropped');
      callback(null);
    });
  });
  migrate_tasks.push(function(callback) {
    if (!mongoose.connection.collections['dictionary_userword'])
      return callback(null);
    mongoose.connection.collections['dictionary_userword'].drop( function(err) {
      console.log('DictionaryUserword collection dropped');
      callback(null);
    });
  });

  migrate_tasks.push(function(callback) {
    DB.users.find({}).all(function(err, list) {
      if (err) {
        return callback(err);
      }
      var counter = 0,
          total = list.length;

      list.forEach(function(item) {
        var data = { 
          login: item.login,
          email: item.email,
          avatar: item.avatar,
          password_hash: item.password,
          provider: item.provider,
          salt: null,
          authToken: null,
          profile: {}
        };
        if (item.profile_id) {
          data.profile.id = parseInt(item.profile_id);
        }
        if (item.provider == 'local') {
          data.password = utils.genToken();
        }
        var new_item = new Users(data);
        new_item.save(function (err, new_item) {
          if (err) {
            callback(err);
          }
          REFS.users[item.id] = new_item;
          console.log('user '+new_item.login);
          counter += 1;
          if (counter == total) {
            callback(null);
          }
        })
      });
    });
  });

  migrate_tasks.push(function(callback) {
    DB.challenges.find({}).all(function(err, list) {
      if (err) {
        return callback(err);
      }

      var i, counter = 0,
          source_results = [],
          source_results_pos = 1,
          source_results_prev_words = null,
          results = [],
          total = list.length;

      list.forEach(function(item) {
        WORDS.push([(item.date.getTime()/1000), item.word]);
        USED_WORDS.push(item.word);

        Challenges.create({ 
          word: item.word,
          date: item.date,
          answers_count: item.answers_count,
          answers_list: JSON.parse(item.answers_list),
          active_to: item.active_to,
          is_active: item.is_active,
          results: {}
        }, 
        function (err, new_challenge) {
          if (err) {
            return callback(err);
          }
          REFS.challenges[item.id] = new_challenge;
          console.log('challenge '+new_challenge.word);

          source_results = JSON.parse(item.results);
          
          if (source_results) 
          {
            source_results_prev_words = null;
            results = [];
            source_results_pos = 1;
            for (i in source_results) 
            {
              if (source_results_prev_words && source_results_prev_words != source_results[i].words) {
                source_results_pos += 1;
              }
              results.push({
                answers_count: source_results[i].words,
                challenge: new_challenge._id.toString(),
                position: source_results_pos,
                sort: new_challenge.date.getTime(),
                user: {
                  avatar: REFS.users[source_results[i].users[0]].avatar,
                  login: REFS.users[source_results[i].users[0]].login
                }
              })
              source_results_prev_words = source_results[i].words;
            }
            new_challenge.results = results;
            new_challenge.save(function(err){
              if (err) {
                return callback(err);
              }
              console.log('challenge results '+new_challenge.word);
              counter += 1;
              if (counter == total) {
                callback(null);
              }
            });
          }
          else {
            counter += 1;
            if (counter == total) {
              callback(null);
            }
          }
        });
      });
    });
  });

  migrate_tasks.push(function(callback) {
    DB.challenges_answers.find({}).all(function(err, list) {
      if (err) {
        return callback(err);
      }

      var counter = 0,
          total = list.length;

      list.forEach(function(item) {
        if (!USERS_WORDS[item.user_id]) {
          USERS_WORDS[item.user_id] = [];
        }
        JSON.parse(item.words_list).forEach(function(word) {
          if (!~USERS_WORDS[item.user_id].indexOf(word)) {
            USERS_WORDS[item.user_id].push(word);
          }
        });

        ChallengesAnswers.create({ 
          challenge: REFS.challenges[item.challenge_id]._id,
          // user: {
          //   _id: REFS.users[item.user_id]._id,
          //   login: REFS.users[item.user_id].login,
          //   avatar: REFS.users[item.user_id].avatar
          // },
          user: REFS.users[item.user_id]._id,
          sort: REFS.challenges[item.challenge_id].date.getTime(),
          answers_list: JSON.parse(item.words_list),
          answers_count: item.words_count
        }, 
        function (err, new_item) {
          if (err) {
            return callback(err);
          }
          console.log('challenge answer '+item.challenge_id+' - '+item.user_id);
          counter += 1;
          if (counter == total) {
            callback(null);
          }
        })
      });
    });
  });

  migrate_tasks.push(function(callback) {
    DB.users_rating.find({}).all(function(err, list) {
      if (err) {
        return console.log(err);
      }

      var i, j, rating, awards, awards_list, words_count,
          counter = 0,
          total = list.length,
          score = config.challenges.score;

      list.forEach(function(item) {

        // var getRandomInt = function(min, max) { return Math.floor(Math.random()*(max-min+1))+min; },
        //     wc = (WORDS.length-1),
        //     awards = {
        //       g: getRandomInt(0, 10),
        //       s: getRandomInt(0, 10),
        //       b: getRandomInt(0, 10),
        //     },
        //     awards_list = { g:[], s:[], b:[] },
        //     rating = 0,
        //     raring_scores = { g:100, s:60, b:30 },
        //     achievements = [],
        //     words_count = getRandomInt(0, 500);

        // for (var type in awards) {
        //   var count = awards[type];
        //   for (var i=0; i<count; i++) {
        //     rating += raring_scores[type];
        //     var w = WORDS[getRandomInt(0, wc)];
        //     awards_list[type].push([ w[0], w[1] ]);
        //   }
        // }
        awards = JSON.parse(item.awards);
        awards_list = JSON.parse(item.awards_list);
        delete awards.t10;
        delete awards_list.t10;

        rating = score[1]*awards.g + score[2]*awards.s + score[3]*awards.b;

        words_count = USERS_WORDS[item.user_id] ? USERS_WORDS[item.user_id].length : 0 ;
        for (i in awards_list) 
        {
          for (j in awards_list[i]) {
            awards_list[i][j][0] *= 1000;
          }
        }
        UsersRating.update(
        {
          user: REFS.users[item.user_id]._id
        },
        { 
          awards : awards,
          awards_list : awards_list,
          rating : rating,
          achievements : JSON.parse(item.achievements),
          words_count : words_count,
          words_list: USERS_WORDS[item.user_id]
        }, 
        function (err, new_item) {
          if (err) {
            return callback(err);
          }
          console.log('user rating '+item.user_id);
          counter += 1;
          if (counter == total) {
            callback(null);
          }
        });
      });
    });
  });

  migrate_tasks.push(function(callback) {
    DB.words_extend.find({}).all(function(err, list) {
      if (err) {
        return console.log(err);
      }

      var date_modify = 500,
          counter = 0,
          total = list.length;

      list.forEach(function(item, idx) {
        DictionaryUserword.create({ 
          word : item.word,
          user: REFS.users[item.user_id]._id,
          user_login: REFS.users[item.user_id].login,
          challenge: REFS.challenges[item.challenge_id]._id,
          token : item.token || utils.genToken(),
          is_apply: !item.token && item.is_accepted && true || false,
          is_discard: !item.token && !item.is_accepted && true || false,
          date: moment().subtract(date_modify+idx, 'm').toDate()
        }, 
        function (err, new_item) {
          if (err) {
            return callback(err);
          }
          counter += 1;
          if (counter == total) {
            callback(null);
          }
        });
      });
    });
  });

  migrate_tasks.push(function(callback) {
    var counter = 0,
        total = USED_WORDS.length;

    USED_WORDS.forEach(function(word) {
      console.log('is_use word '+word);
      Dictionary.findOneAndUpdate({ word:word }, { $set:{ is_use:true } }, function(err, upd_word) {
        if (err) {
          return callback(err);
        }
        counter += 1;
        if (counter == total) {
          callback(null);
        }
      });
    });
  });

  console.log('run');

  async.waterfall(migrate_tasks, function (err, result) {
    if (err)
      console.log(err);
    else
      console.log('done');
  });
}