module.exports = function (db, callback) {

  var Users = db.define('users', {
    login : String,
    email : String,
    password : String,
    avatar : String,
    provider : String,
    profile_id : String,
    restore_token : String,
    activate_token : String
  });

  var UsersRating = db.define('users_rating', {
    user_id : Number,
    awards : String,
    awards_list : String,
    rating : Number,
    achievements : String,
    words_count : Number
  });

  var UsersWords = db.define('users_words', {
    user_id : Number,
    words : String
  });

  var Challenges = db.define('challenges', {
    word : String,
    answers_list : String,
    date : Date,
    active_to : Date,
    is_active : Boolean,
    results : String
  });

  var ChallengesAnswers = db.define('challenges_answers', {
    challenge_id : Number,
    user_id : Number,
    user_login : String,
    words_list : String,
    words_count : Number
  });

  var Words = db.define('words', {
    word : String,
    index : String,
    length : Number,
    type : String,
    type_raw : String,
    matches_list : String,
    matches_count : Number,
    compare_count : Number,
    compare_time : Number,
    value : String,
    in_use : Boolean,
    is_next : Boolean
  });

  var WordsExtend = db.define('words_extend', {
    user_id : Number,
    challenge_id : Number,
    is_accepted : Number,
    token : String,
    word : String
  });

  return callback();
};