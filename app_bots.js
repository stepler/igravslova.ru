var path = require('path');
global.config = require('./config/config')[(process.env.NODE_ENV || 'development')];

var config = global.config;
require('./config/db')(config);

require(path.join(config.path.conf, 'logger'))(config);

var cronJob = require('cron').CronJob;

var bots_list = [],
    bots_config = require('./config/bots'),
    bots_player = require(path.join(config.path.lib, 'bots'));

bots_config.forEach(function(item) {
  bots_list.push(new bots_player(item.user, item.propability, item.answers));
});

function main()
{
  // bots_list.forEach(function(item) {
  //   item.tryToPlay();
  // });
  // return;
  new cronJob('0 */10 * * * *', function(){
    bots_list.forEach(function(item) {
      item.tryToPlay();
    });
  }, null, true);
}

if (require.main === module) {
  setTimeout(main, 1000);
}