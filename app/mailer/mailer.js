var config = global.config,
    path = require('path');

var _ = require('lodash'),
    nodemailer = require('nodemailer')
    moment = require('moment'),
    swig = require('swig'),
    utils = require(path.join(config.path.lib, 'utils'));

var smtpTransport = null;
function initTransport() {
  if (smtpTransport)
    return;

  smtpTransport = nodemailer.createTransport('SMTP',{
    service: config.mail.smtp.provider,
    auth: {
      user: config.mail.smtp.user,
      pass: config.mail.smtp.pass
    },
    maxConnections: 1
  });
}
function closeTransport()
{
  if (smtpTransport)
    smtpTransport.close();
}

var mailOptions = {
  from: config.mail.fromEmail
}

// swig.setDefaults({ locals:{ config:config } });

var confirmEmailRequestTpl = swig.compileFile( path.join(config.path.mail_tpl, 'confirm-email-request.html') );
var registerTpl = swig.compileFile( path.join(config.path.mail_tpl, 'register.html') );
var restoreRequestTpl = swig.compileFile( path.join(config.path.mail_tpl, 'restore-request.html') );
var restoreTpl = swig.compileFile( path.join(config.path.mail_tpl, 'restore.html') );
var extendDictTpl = swig.compileFile( path.join(config.path.mail_tpl, 'extend-dict.html') );
var extendDictAcceptTpl = swig.compileFile( path.join(config.path.mail_tpl, 'extend-dict-accept.html') );
var extendDictDiscardTpl = swig.compileFile( path.join(config.path.mail_tpl, 'extend-dict-discard.html') );
var Award1Tpl = swig.compileFile( path.join(config.path.mail_tpl, 'award-1.html') );
var Award2Tpl = swig.compileFile( path.join(config.path.mail_tpl, 'award-2.html') );
var Award3Tpl = swig.compileFile( path.join(config.path.mail_tpl, 'award-3.html') );
var Feedback = swig.compileFile( path.join(config.path.mail_tpl, 'feedback.html') );

var send_mail = function(mail_to, subject, message, callback)
{
  initTransport();
  smtpTransport.sendMail(
    _.merge(mailOptions, {
      to: mail_to,
      subject: subject,
      html: message
    }), 
    function(error, response){
      if (callback)
        callback();
      else
        closeTransport();
  });
}
module.exports = {
  register: function(user)
  {
    if (!user.email)
      return;

    send_mail(
      user.email,
      'Вэлком - '+config.app.host,
      registerTpl(),
      function()
      {
        send_mail(
          user.email,
          'Подтверждение E-mail - '+config.app.host,
          confirmEmailRequestTpl(user)
        );
      }
    );
  },
  confirmEmailRequest: function(user)
  {
    send_mail(
      user.email,
      'Подтверждение E-mail - '+config.app.host,
      confirmEmailRequestTpl(user)
    );
  },
  restoreRequest: function(user)
  {
    send_mail(
      user.email,
      'Восстановление пароля - '+config.app.host,
      restoreRequestTpl(user)
    );
  },
  restore: function(user)
  {
    send_mail(
      user.email,
      'Пароль восстановлен - '+config.app.host,
      restoreTpl()
    );
  },
  extend_dict: function(login, word, token)
  {
    send_mail(
      config.mail.adminEmail,
      'Запрос на добавление слова - '+config.app.host,
      extendDictTpl({ login:login, word:word })
    );
  },
  extend_dict_accept: function(mail_to, login, word)
  {
    var smtpTransport = getTransport();
    smtpTransport.sendMail(
      _.merge(mailOptions, {
        to: mail_to,
        subject: 'Добавление новго слова в словарь - '+config.app.host,
        html: extendDictAcceptTpl({ login:login, word:word })
      }), 
      function(error, response){
        smtpTransport.close();
    });
  },
  extend_dict_discart: function(mail_to, login, word)
  {
    var smtpTransport = getTransport();
    smtpTransport.sendMail(
      _.merge(mailOptions, {
        to: mail_to,
        subject: 'Добавление новго слова в словарь - '+config.app.host,
        html: extendDictDiscardTpl({ login:login, word:word })
      }), 
      function(error, response){
        smtpTransport.close();
    });
  },
  award_1: function(user, word, game_link)
  {
    send_mail(
      user.email,
      'Та-дам!!! Вы выиграли золото! '+config.app.host,
      Award1Tpl({ word:word, word_link:game_link })
    );
  },
  award_2: function(user, word, game_link)
  {
    send_mail(
      user.email,
      'Вы выиграли серебро! '+config.app.host,
      Award1Tpl({ word:word, word_link:game_link })
    );
  },
  award_3: function(user, word, game_link)
  {
    send_mail(
      user.email,
      'Вы выиграли бронзу! '+config.app.host,
      Award3Tpl({ word:word, word_link:game_link })
    );
  },
  feedback: function(subject, email, message)
  {
    send_mail(
      config.mail.adminEmail,
      'Feedback from '+config.app.host,
      Feedback({ subject:subject, email:email, message:message })
    );
  }
}