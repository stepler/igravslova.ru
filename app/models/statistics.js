var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Schema = mongoose.Schema;

var StatisticsSchema = new Schema({
  user: { 
    _id: { type: Schema.Types.ObjectId, ref: 'Users' },
    login: String
  },
  challenge: { 
    _id: { type: Schema.Types.ObjectId, ref: 'Challenges' },
    word: String,
    date: Date
  },
  date: Date
}, {
  collection: 'statistics',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { 
      ret.challenge.date = ret.challenge.date.getTime();
      ret.date = ret.date.getTime();
      delete ret._id;
    }
  }
});

mongoose.model('Statistics', StatisticsSchema);