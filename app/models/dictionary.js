var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Schema = mongoose.Schema;

var DictionarySchema = new Schema({
  word: String,
  index: String,
  length: Number,
  type: String,
  type_raw: String,
  is_use: { type:Boolean, default:false },
  is_next: { type:Boolean, default:false },
  answers_list: { type:Array, default:[] }
}, {
  collection: 'dictionary',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { delete ret._id; }
  }
});

var DictionaryUserwordSchema2 = new Schema({
  word: String,
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  user_login: String,
  challenge: { type: Schema.Types.ObjectId, ref: 'Challenges' },
  date: { type: Date, default: Date.now },
  token: String,
  in_process: { type:String, default:'none' },
  is_apply: { type:Boolean, default:false },
  is_discard: { type:Boolean, default:false }
}, {
  collection: 'dictionary_userword',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { delete ret._id; }
  }
});

mongoose.model('Dictionary', DictionarySchema);
mongoose.model('DictionaryUserword', DictionaryUserwordSchema2);
