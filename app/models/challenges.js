var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Schema = mongoose.Schema;

var ChallengesSchema = new Schema({
  word: String,
  date: Date,
  answers_list: [],
  answers_count: { type: Number, default: 0 },
  active_to: Date,
  is_active: { type: Boolean, default: true },
  results: {}
}, {
  collection: 'challenges',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.date = ret.date.getTime();
      ret.active_to = ret.active_to.getTime();
      delete ret._id; 
      delete ret.answers_list; 
    }
  }
});

ChallengesSchema.pre('save', function(next) {
  if (!this.isNew) {
    return next();
  }
  this.answers_count = this.answers_list.length;
  next();
});

var ChallengesAnswersSchema = new Schema({
  challenge: { type: Schema.Types.ObjectId, ref: 'Challenges' },
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  sort: Number,
  // user: { 
  //   _id: { type: Schema.Types.ObjectId, ref: 'Users' },
  //   login: String,
  //   avatar: String
  // },
  answers_list: [],
  answers_count: { type: Number, default: 0 },
}, {
  collection: 'challenges_answers',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { delete ret._id; }
  }
});

ChallengesAnswersSchema.pre('save', function(next) {
  if (!this.isNew) {
    return next();
  }
  this.answers_count = this.answers_list.length;
  next();
});

ChallengesAnswersSchema
  .virtual('position')
  .set(function(position) { this._position = position; })
  .get(function() { return this._position })

mongoose.model('Challenges', ChallengesSchema);
mongoose.model('ChallengesAnswers', ChallengesAnswersSchema);
