var config = global.config,
    path = require('path');

var mongoose = require('mongoose'),
    crypto = require('crypto');

var Schema = mongoose.Schema;

var UsersSchema = new Schema({
  provider: String,
  reg_date: { type: Date, default: Date.now },
  login: String,
  email: { type: String, default: '' },
  is_confirmed_email: { type: Boolean, default: false },
  avatar: String,
  password_hash: String,
  salt: String,
  restore_token: String,
  email_confirm_token: String,
  profile: {}
}, {
  collection: 'users',
  id: false,
  toJSON: { 
    transform: function(doc, ret, options) { 
      delete ret._id; 
      delete ret.password_hash; 
      delete ret.salt; 
      delete ret.is_confirmed_email; 
      delete ret.restore_token; 
      delete ret.email_confirm_token; 
      delete ret.profile; 
    }
  }
});

UsersSchema
  .virtual('password')
  .set(function(password) {
    if (password) {
      this._password = password;
      this.salt = this.make_salt();
      this.password_hash = this.encrypt_password(password);
    }
  })
  .get(function() { return this._password });

UsersSchema.pre('save', function(next) {
  this.wasNew = this.isNew;
  next();
});

UsersSchema.post('save', function(user) {
  if (this.wasNew) {
    var UsersRating = mongoose.model('UsersRating');
    UsersRating.create({ user:user._id });
  }
})

UsersSchema.methods = {

  is_correct_password: function(plainText) 
  {
    return this.encrypt_password(plainText) === this.password_hash;
  },

  make_salt: function() 
  {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  },

  encrypt_password: function(password) 
  {
    try {
      if (!password) {
        return '';
      }
      return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    } catch (err) {
      return '';
    }
  }
};


var UsersRatingSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  // user: { 
  //   _id: { type: Schema.Types.ObjectId, ref: 'Users' },
  //   login: String,
  //   avatar: String
  // },
  awards: {},
  awards_list: {},
  rating: { type: Number, default: 0 },
  achievements: {},
  words_count: { type: Number, default: 0 },
  words_list: {}
}, {
  collection: 'users_rating',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { delete ret._id; }
  }
});

UsersRatingSchema
  .virtual('position')
  .set(function(position) { this._position = position; })
  .get(function() { return this._position })
  

// Middleware
UsersRatingSchema.pre('save', function(next) {
  if (!this.isNew) return next();
  if (!this.awards) { this.awards = { g:0, s:0, b:0 }; }
  if (!this.awards_list) { this.awards_list = { g:[], s:[], b:[] }; }
  if (!this.achievements) { this.achievements = []; }
  if (!this.rating) { this.rating = 0; }
  if (!this.words_count) { this.words_count = 0; }
  if (!this.words_list) { this.words_list = []; }
    
  next();
});

mongoose.model('Users', UsersSchema);
mongoose.model('UsersRating', UsersRatingSchema);
