var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Schema = mongoose.Schema;

var NotifySchema = new Schema({
  user:{ type: Schema.Types.ObjectId, ref: 'Users' },
  title: String,
  message: String,
  date: { type: Date, default: Date.now },
  is_read: { type: Boolean, default: false }
}, {
  collection: 'notify',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { 
      ret.date = ret.date.getTime();
      delete ret._id; 
    }
  }
});

var NotifyGlobalSchema = new Schema({
  title: String,
  message: String,
  date: { type: Date, default: Date.now }
}, {
  collection: 'notify_global',
  id: false,
  toJSON: { 
    virtuals: true,
    transform: function(doc, ret, options) { 
      ret.date = ret.date.getTime();
      delete ret._id; 
    }
  }
});

mongoose.model('Notify', NotifySchema);
mongoose.model('NotifyGlobal', NotifyGlobalSchema);