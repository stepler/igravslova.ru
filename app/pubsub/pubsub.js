var _ = require('lodash'),
    redis = require('redis');

exports.init_publisher = function(queue)
{
  var redis_client = redis.createClient();
  return function(data) {
    setTimeout(function() {
      redis_client.publish(queue, JSON.stringify(data));
    }, 0);
  }
}

exports.init_subscriber = function(queue, callback)
{
  var redis_client = redis.createClient();
  redis_client.subscribe(queue);
  redis_client.on("message", function(channel, data) {
    callback(channel, JSON.parse(data));
  });
}