var config = global.config,
    logger = global.logger,
    path = require('path');

var mongoose = require('mongoose'),
    moment = require('moment'),
    markdown = require( "markdown" ).markdown,
    pubsub = require(path.join(config.path.pubsub, 'pubsub')),
    notify_module = require(path.join(config.path.modules, 'notify'));

exports.sockets = function() {
  
  var connection_pool = {};

  pubsub.init_subscriber(config.pubsub.notify, function(channel, data) {
    if (connection_pool[data.user_id]) {
      return connection_pool[data.user_id].emit(config.pubsub.notify, { t:data.title, m:data.message });
    }
    notify_module.save_message(data.user_id, data.title, data.message, function(){});
  });

  function send_unread_messages(user_id) 
  {
    notify_module.get_messages(user_id, true, function(err, list) {
      list && list.forEach(function(item) {
        connection_pool[user_id].emit(config.pubsub.notify, 
          { t:item.title, m:markdown.toHTML(item.message), d:item.date.getTime() });
      });
    });
  }

  return {
    connection: function(socket, user_id) {
      connection_pool[user_id] = socket;
      send_unread_messages(user_id);
    },

    disconnect: function(socket, user_id) {
      delete connection_pool[user_id];
    }
  }
};

exports.global_messages = function(req, res)
{
  var timestamp = req.cookies[config.notify.cookie_name],
      date = timestamp ? moment(parseInt(timestamp)) : moment().subtract(config.notify.active_days, 'days');

  notify_module.get_global_messages(date, function(err, messages) {
    if (err) {
      logger.error(err);
      return res.json(500);
    }
    var result = [];
    messages.forEach(function(item) {
      result.push({ t:item.title, m:markdown.toHTML(item.message), d:item.date.getTime() });
    });
    res.cookie(config.notify.cookie_name, 
      (new Date()).getTime(), { expires:moment().add(config.notify.active_days, 'days').toDate() });
    return res.json(200, { messages:result });
  });
}