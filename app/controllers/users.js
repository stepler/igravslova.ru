var config = global.config,
    path = require('path');

var _ = require('lodash'),
    passport = require('passport'),
    utils = require('../../lib/utils'),
    mongoose = require('mongoose'),
    Users = mongoose.model('Users'),
    user_module = require(path.join(config.path.modules, 'user')),
    mailer = require(path.join(config.path.mail, 'mailer'));

exports.login = function(req, res)
{
  req.validateBody('email', 'empty_email', 'notEmpty');
  req.validateBody('password', 'empty_password', 'notEmpty');

  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }
  // Users.findOne({ login:"Евгения Сапелкина" }, function(err, user) {
  //   if (err) {
  //     var error = utils.parseErrors(err);
  //     return res.json(error.status, error.message);
  //   }
  //   req.logIn(user, function(err) {
  //     if (err) {
  //       var error = utils.parseErrors(err);
  //       return res.json(error.status, error.message);
  //     }
  //     return res.json(200, { redirect:(req.session.returnTo || '/') });
  //   });
  // });
  // return;
  user_module.authenticate(req.body.email, req.body.password, function(err, user) {
    if (err) {
      var error = utils.parseErrors(err, 'error_on_login', 'email');
      return res.json(error.status, error.message);
    }
    req.logIn(user, function(err) {
      if (err) {
        var error = utils.parseErrors(err, 'error_on_login', 'email');
        return res.json(error.status, error.message);
      }
      return res.json(200, { redirect:(req.session.returnTo || '/') });
    });
  });
}

exports.authenticate = function(strategy)
{
  return function(req, res, next) {
    passport.authenticate(strategy, function(err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.redirect('/users/login/'+strategy+'-error/');
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        if (user.email)
          return res.redirect('/');
        return res.redirect('/#/users/request-email');
      })
    })(req, res, next);
  }
}

exports.register = function(req, res) 
{
  // защита от ботов (примитивная)
  if (_.has(req.body, 'legal_agree')) {
    return res.json(200, { redirect:'/' });
  }
  req.validateBody('login', 'empty_login', 'notEmpty');
  req.validateBody('email', 'empty_email', 'notEmpty');
  req.validateBody('email', 'invalid_email', 'isEmail');
  req.validateBody('password', 'empty_password', 'notEmpty');
  req.validateBody('confirm', 'empty_confirm', 'notEmpty');
  req.validateBody(['confirm', 'password'], 'not_equals', 'equals');
  
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  user_module.register(req.body.login, req.body.email, req.body.password, 
    'local', {}, function(err, user) {

    if (err) {
      var error = utils.parseErrors('error_on_register', 'email');
      return res.json(error.status, error.message);
    }
    mailer.register(user);
    // setTimeout(function(){
    //   mailer.confirmEmailRequest(user);
    // }, 100)
    req.logIn(user, function(err) {
      if (err) {
        var error = utils.parseErrors('error_on_login', 'email');
        return res.json(error.status, error.message);
      }
      return res.json(200, { redirect:(req.session.returnTo || '/') });
    });
  })
}

exports.update = function(req, res)
{
  if (!req.isAuthenticated()) {
    return res.json(403, { location: '/403' }); 
  }
  // var user_id = req.params.user_id;
  // if (user_id == 'me') {
    user_id = req.user._id;
  // }

  req.validateBody('login', 'empty_login', 'notEmpty');
  req.validateBody('email', 'empty_email', 'notEmpty');
  req.validateBody('email', 'invalid_email', 'isEmail');
  // if (!_.isEmpty(req.body.password)) {
  //   req.validateBody('confirm', 'empty_confirm', 'notEmpty');
  //   req.validateBody(['confirm', 'password'], 'not_equals', 'equals');
  // }
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  var user_data = {
    login: req.body.login,
    email: req.body.email,
    // password: req.body.password
  }

  user_module.update_profile(user_id, user_data, req.body.current_password||null, function(err, user) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    if (user && !user.is_confirmed_email) {
      mailer.confirmEmailRequest(user);
    }
    return res.json(200, {});
  });
}

exports.restoreRequest = function(req, res)
{
  if (req.isAuthenticated()) {
    return res.json(302, { location: '/' }); 
  }
  req.validateBody('email', 'empty_email', 'notEmpty');
  req.validateBody('email', 'invalid_email', 'isEmail');
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  user_module.restore_password(req.body.email, null, function(err, user) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    mailer.restoreRequest(user);
    return res.json(200, {});
  });
}

exports.restore = function(req, res)
{
  if (req.isAuthenticated()) {
    return res.json(302, { location: '/' }); 
  }

  req.validateBody('password', 'empty_password', 'notEmpty');
  req.validateBody('confirm', 'empty_confirm', 'notEmpty');
  req.validateBody(['confirm', 'password'], 'not_equals', 'equals');
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  user_module.restore_password(req.body.token, req.body.password, function(err, user) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, {});
  });
}

exports.emailRequest = function(req, res)
{
  if (!req.isAuthenticated()) {
    return res.json(403, { location: '/403' }); 
  }
  user_id = req.user._id;

  req.validateBody('email', 'empty_email', 'notEmpty');
  req.validateBody('email', 'invalid_email', 'isEmail');
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  var user_data = {
    email: req.body.email
  }

  user_module.update_profile(user_id, user_data, null, function(err, user) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    mailer.confirmEmailRequest(user);
    return res.json(200, {});
  });
}

exports.emailConfirm = function(req, res)
{
  user_module.confirm_email(req.query.token, function(err, user) {
    return res.redirect('/');
  });
}