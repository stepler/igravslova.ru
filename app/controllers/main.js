var config = global.config,
    path = require('path'),
    fs = require('fs');

var _ = require('lodash'),
    moment = require('moment'),
    markdown = require( "markdown" ).markdown,
    mailer = require(path.join(config.path.mail, 'mailer')),
    notify_module = require(path.join(config.path.modules, 'notify'));

exports.index = function(req, res)
{
  var timestamp = req.cookies[config.notify.cookie_name],
      date = timestamp ? moment(parseInt(timestamp)) : moment().subtract(config.notify.active_days, 'days');

  notify_module.isset_global_messages(date, function(err, have_messages) {
    res.render('index', { have_messages:have_messages });
  });
}

exports.content = function(req, res)
{
  if (req.route.path == '/content/rules')
  {
    fs.readFile(
      path.join(config.path.views, 'content', 'rules.markdown'), 
      function (err, data) {
        if (err) {
          return res.json(500);
        }
        return res.json(200, { title:'<i class="b-list__title-icon icon-help"></i>Правила игры', text:markdown.toHTML(data.toString()) });
      }
    );
  }
  else
    return res.json(404);
}

exports.feedback = function(req, res)
{
  // защита от ботов (примитивная)
  if (_.has(req.body, 'legal_agree')) {
    return res.json(200, { redirect:'/' });
  }

  req.validateBody('feedback_subject', 'empty_subject', 'notEmpty');
  req.validateBody('feedback_email', 'empty_email', 'notEmpty');
  req.validateBody('feedback_message', 'empty_message', 'notEmpty');
  
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  mailer.feedback(req.body.feedback_subject, 
    req.body.feedback_email, markdown.toHTML(req.body.feedback_message));
  return res.json(200);
}