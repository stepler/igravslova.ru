var config = global.config,
    logger = global.logger,
    path = require('path'),
    fs = require('fs');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    sitemap = require('sitemap'),
    utils = require(path.join(config.path.lib, 'utils')),
    redisCache = require(path.join(config.path.lib, 'redis_cache')),
    user_module = require(path.join(config.path.modules, 'user')),
    challenge_module = require(path.join(config.path.modules, 'challenge')),
    rating_module = require(path.join(config.path.modules, 'rating')),
    dictionary_module = require(path.join(config.path.modules, 'dictionary')),
    achievements = require(path.join(config.path.conf, 'achievements')),
    mailer = require(path.join(config.path.mail, 'mailer'));

var Challenges = mongoose.model('Challenges');

exports.completeChallenge = function(callback) {
  var today = moment({hour: 0, minute: 0, second: 0, millisecond: 0});

  Challenges.find({ 'active_to':{ $lte:today.toDate() }, 'is_active':true }, 
    { __v:0, answers_list:0 }).lean().exec(function(err, list) {

    if (err) {
      return callback(err);
    }
        
    list.forEach(function(item) {
      challenge_module.game({ challenge:item }, 
        { rating:true }, function(err, challenge, user_answers, challenge_rating) {

        if (err) {
          return callback(err);
        }

        var asyncTasks = [],
            winners = challenge_rating.filter(function(item){ return item.position <= 3 });

        winners.forEach(function(winner) {
          asyncTasks.push(function(callback) {
            rating_module.add_award(winner.position, winner.user, challenge, function(err) {
              callback(err, null);
            });
          });
        });

        async.parallel(asyncTasks, function(err) {
          // var rating_result = _.map(challenge_rating, function(item){ return item.toJSON() });
          challenge_module.complete_challenge(challenge._id, challenge_rating, function(err) {
            if (err) {
              return callback(err);
            }

            winners.forEach(function(winner) {
              user_module.get_user(winner.user._id, function(err, user) {
                if (err) {
                  return callback(err);
                  // return logger.error(err);
                }

                var game_link = '/game/'+moment(challenge.date).format('D.MM.YYYY');
                if (winner.position == '1')
                  mailer.award_1(user, challenge.word, game_link);
                if (winner.position == '2')
                  mailer.award_2(user, challenge.word, game_link);
                if (winner.position == '3')
                  mailer.award_3(user, challenge.word, game_link);
              })
            });
            
            var msg = 'End Challenge: %word%'
                  .replace('%word%', challenge.word);
            return callback(null, msg);
            // return logger.info(msg);
          });
        });
      });
    });
  });
}

exports.findNextChallenge = function(callback) 
{
  dictionary_module.find_next_challenge(function(err, next_word) {
    if (err) {
      return callback(err);
    }
    if (!next_word) {
      return callback(null, null);
    }
    var msg = 'Next Challenge: %word% (%count%)'
              .replace('%word%', next_word.word)
              .replace('%count%', next_word.answers_list.length);
    return callback(null, msg);
  })
}

exports.addChallenge = function(callback) 
{
  dictionary_module.add_challenge(function(err, new_challenge, word) {
    if (err) {
      return callback(err);
      // return logger.error(err);
    }
    var msg = 'New Challenge: %word% (%count%) %date_from% - %date_to%'
              .replace('%word%', new_challenge.word)
              .replace('%count%', new_challenge.answers_list.length)
              .replace('%date_from%', moment(new_challenge.date).format('D.MM.YYYY'))
              .replace('%date_to%', moment(new_challenge.active_to).format('D.MM.YYYY'));
    return callback(err, msg);
  });
}


exports.addWord = function(user_id, new_word, callback) {
  var self = this;
  redisCache.getUserRating(user_id, ['words_count', 'words_list'], 
    function(err, list) {
      if (err || ~list.words_list.indexOf(new_word)) {
        return callback(err, false);
      }
      rating_module.update_word_stat(user_id, new_word, function(err, user_rating) {
        if (err || !result) {
          return callback(err, result);
        }
        redisCache.setUserRating(user_id, ['words_count', 'words_list'], user_rating);
        return callback(null, true);
      });
    }
  );
}

exports.checkAchievements = function(user_id, type, callback) {
  var self = this;
  if (type == 'words') {
    /*
    redisCache.getUserRating(user_id, ['words_count', 'achievements'],
      function(err, list) {
        if (err) {
          return callback.apply(self, [err, false]);
        }
        achievements.words.get(
          list.words_count, (list.achievements.words || []), 
          function(added_list, all_list) {
            if (!added_list.length) {
              return callback.apply(self, [null, null]);
            }
            UsersRating.findOneAndUpdate(
              { user:user_id },
              { $setOnInsert:{ 'achievements.words':all_list }},
              function(err, user) {
                if (err) {
                  return callback.apply(self, [err, null]);
                }
                redisCache.setUserRating(user_id, ['achievements'], user);
                return callback.apply(self, [null, added_list]);
              }
            );
          }
        );
      }
    );*/
  }
  else {
    return callback(null, null);
  }
}

exports.extendDict = function(user_id, challenge_id, word, callback) {
  var self = this;
  
  async.series({
    dictionary: function (callback) 
    {
      dictionary_module.extend_dict(word, user_id, function(err, new_word) {
        if (err || !new_word) {
          return callback(err, false);
        }
        return callback(null, true);
      })
    },
    challenges: function(callback) 
    {
      challenge_module.extend_challenge_answers(word, function(err) {
        if (err) {
          return callback(err, false);
        }
        return callback(null, true);
      });
    },
    add_game_answer: function(callback) 
    {
      var filter = {
        user_id: user_id,
        challenge_id: challenge_id
      };

      challenge_module.add_game_answer(filter, word, function(err) {
        if (err) {
          return callback(err, false);
        }
        return callback(null, true);
      });
    }
  },
  function(err, result) {
    if (err) {
      return callback(err);
    }
    return callback(null, result);
  });
}

exports.updateSitemap = function(callback) {
  var counter = 0,
      sitemap_file = path.join(config.path.public, 'sitemap.xml'),
      date = moment(),
      end_timestamp = config.challenges.first_challenge,
      sm = sitemap.createSitemap ({
        hostname: config.app.httphost,
        urls: [
          { url: '/#!/',  changefreq: 'daily', priority: 1.0 },
          { url: '/#!/rating',  changefreq: 'daily',  priority: 0.8 },
          { url: '/#!/games',  changefreq: 'daily',  priority: 0.6 },
          { url: '/#!/rules',  changefreq: 'daily',  priority: 0.5 },
          { url: '/#!/feedback',  changefreq: 'monthly',  priority: 0.5 }
        ]
      });

  while (date.valueOf() > end_timestamp) {
    counter++;
    if (counter <= 30) {
      sm.add({ url:'/#!/game/'+date.format('DD.MM.YYYY'), changefreq:'daily', priority:0.8 });
    }
    else if (counter <= 180) {
      sm.add({ url:'/#!/game/'+date.format('DD.MM.YYYY'), changefreq:'monthly', priority:0.5 });
    }
    else {
      sm.add({ url:'/#!/game/'+date.format('DD.MM.YYYY'), changefreq:'yearly', priority:0.1 });
    }
    date.subtract(1, 'days');
  }

  fs.writeFile(sitemap_file, sm.toString(), function (err,data) {
    if (err) {
      return callback(err);
    }
    var msg = 'Sitemap save: (%count%) (%file%)'
              .replace('%count%', counter)
              .replace('%file%', sitemap_file);
    callback(null, msg)
  });
}