var config = global.config,
    logger = global.logger,
    path = require('path');

var mongoose = require('mongoose'),
    moment = require('moment'),
    utils = require(path.join(config.path.lib, 'utils')),
    rating_module = require(path.join(config.path.modules, 'rating'));
    // UsersRating = mongoose.model('UsersRating')
    // utils = require('../../lib/utils');

exports.list = function(req, res)
{
  var filter = {},
      pager = {};

  if (req.query.limit !== '0') {
    pager.limit = (req.query.limit > 0 ? parseInt(req.query.limit) : 10);
    pager.page = (req.query.page > 0 ? parseInt(req.query.page) : 1);
  }

  if (req.query.page == '0') {
    pager.no_pagination = true;
  }

  rating_module.rating_list(filter, {}, pager, function(err, rating, pagination) {
    if (err) {
      logger.error(err);
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, { rating: rating, pagination: pagination });
  });
}

exports.get = function(req, res)
{
  var user_id = req.params.user_id;
  if (user_id == 'me') {
    if (!req.isAuthenticated()) {
      return res.json(403, { location: '/403' }); 
    }
    user_id = req.user._id;
  }

  rating_module.rating_user(user_id, function(err, user) {
    if (err) {
      logger.error(err);
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, { user: user });
  });
}
