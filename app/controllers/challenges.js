var config = global.config,
    path = require('path');

var mongoose = require('mongoose'),
    moment = require('moment'),
    redis = require('redis'),
    utils = require(path.join(config.path.lib, 'utils')),
    pubsub = require(path.join(config.path.pubsub, 'pubsub')),
    dictionary_module = require(path.join(config.path.modules, 'dictionary')),
    challenge_module = require(path.join(config.path.modules, 'challenge')),
    mailer = require(path.join(config.path.mail, 'mailer'));

exports.list = function(req, res)
{
  var filter = {},
      pager = {};

  if (req.query.limit !== '0') {
    pager.limit = (req.query.limit > 0 ? parseInt(req.query.limit) :10);
    pager.page = (req.query.page > 0 ? parseInt(req.query.page) :1);
  }

  if (req.query.page == '0') {
    pager.no_pagination = true;
  }

  if (req.query.year) {
    filter.year = parseInt(req.query.year);
    if (req.query.month) {
      filter.month = parseInt(req.query.month);
    }
  }

  challenge_module.list(filter, {}, pager, function(err, challenges, pagination) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, { challenges:challenges, pagination:pagination });
  });
}

exports.get = function(req, res)
{
  var date = moment(req.params.date, 'DD.MM.YYYY'),
      filter = {},
      req_data = {
        user: req.isAuthenticated() ? req.user._id : 0,
        rating: true
      };

  if (!date.isValid()) {
    var error = utils.parseErrors(null, 'invalid_date', 'challenge');
    return res.json(error.status, error.message);
  }

  filter.date = date.toDate();
  challenge_module.game(filter, req_data, function(err, challenge, answers, rating) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, { challenge:challenge, answers:answers, rating:rating, current_time:(new Date()) });
  });
}

var add_word_publisher = pubsub.init_publisher(config.pubsub.add_word);

exports.add_answer = function(req, res)
{
  if (!req.isAuthenticated()) {
    var error = utils.parseErrors(null, 'user_not_auth', 'answer');
    return res.json(403, error.message);
  }

  var date = moment(req.params.date, 'DD.MM.YYYY'),
      user_id = req.user._id,
      filter = { user_id:user_id };

  if (!date.isValid()) {
    var error = utils.parseErrors(null, 'invalid_date', 'answer');
    return res.json(error.status, error.message);
  }
  filter.date = date.toDate();

  req.validateBody('answer', 'empty_answer', 'notEmpty');
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  challenge_module.add_game_answer(filter, req.body.answer, function(err, challenge, answer, user_answers, rating) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    add_word_publisher({ user:user_id, word:answer, challenge:challenge_module._id });
    return res.json(200, { rating:rating });
  });
}

exports.extend = function(req, res)
{
  if (!req.isAuthenticated()) {
    var error = utils.parseErrors(null, 'user_not_auth', 'answer');
    return res.json(403, error.message);
  }

  var date = moment(req.params.date, 'DD.MM.YYYY'),
      filter = {},
      req_data = { user:false, rating:false };

  if (!date.isValid()) {
    var error = utils.parseErrors(null, 'invalid_date', 'challenge');
    return res.json(error.status, error.message);
  }
  filter.date = date.toDate();

  req.validateBody('ext_word', 'empty_word', 'notEmpty');
  var err = req.validationErrors();
  if (err) {
    var error = utils.parseErrors(err);
    return res.json(error.status, error.message);
  }

  challenge_module.game(filter, req_data, function(err, challenge, answers, rating) {
    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    dictionary_module.add_userword(req.body.ext_word, req.user, challenge, function(err, userword) {
      mailer.extend_dict(req.user.login, userword.word, userword.token);
      // redisClient.publish(config.pubsub.extend_word, 
      //   JSON.stringify({ 
      //     user: user_id, 
      //     challenge: challenge_module._id, 
      //     word: req.body.ext_word, 
      //     description: req.body.ext_description
      //   })
      // );
      return res.json(200, {});
    });
  });
}

exports.list_answers = function(req, res)
{
  if (!req.isAuthenticated()) {
    var error = utils.parseErrors(null, 'user_not_auth', 'list_answers');
    return res.json(403, error.message);
  }

  var filter = {}
      pager = {},
      user_id = req.params.user_id;

  if (user_id == 'me') {
    user_id = req.user._id;
  }

  filter.user_id = user_id;

  if (req.query.limit !== '0') {
    pager.limit = (req.query.limit > 0 ? parseInt(req.query.limit) :10);
    pager.page = (req.query.page > 0 ? parseInt(req.query.page) :1);
  }

  if (req.query.page == '0') {
    pager.no_pagination = true;
  }

  challenge_module.games_list(filter, {}, { challenge:true }, 
    pager, function(err, challenges, pagination) {

    if (err) {
      var error = utils.parseErrors(err);
      return res.json(error.status, error.message);
    }
    return res.json(200, { challenges:challenges, pagination:pagination });
  });
}