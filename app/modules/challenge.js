var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Challenges = mongoose.model('Challenges'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers');


var challenge_module = {

  /**
   * Добавить новое слово в игру
   */
  add_new: function(challenge_word, challenge_answers, callback)
  {
    var date = moment(config.challenges.active_time_from),
        active_to = moment(config.challenges.active_time_to).add(config.challenges.active_days, 'days');

    Challenges.create({ 
      word: challenge_word,
      date: date.toDate(),
      active_to: active_to.toDate(),
      answers_list: challenge_answers,
      is_active: true,
      results: {}
    }, 
    callback);
  },

  /**
   * Завершить игру, запивать результаты
   */
  complete_challenge: function(challenge_id, result_json, callback)
  {
    Challenges.findOneAndUpdate(
      { _id:challenge_id },
      { results:result_json, is_active:false }
    )
    .lean().exec(callback);
  },

  /**
   * Получить список слов в игре
   */
  list: function(filter, sort, pager, callback)
  {
    var criteria = {};

    if (filter.year) {
      var date_start = moment([filter.year, (filter.month || 1)-1, 1, 0, 0, 0, 0]).toDate();
      var date_end  = moment(date_start).add((filter.month ? 'months' : 'years'), 1).toDate();
      criteria.date = { $gte: date_start, $lt: date_end };
    }

    var query = Challenges.find(criteria, { __v:0, answers_list:0 });

    if (pager.limit) {
      query.limit(pager.limit).skip(pager.limit * ((pager.page||1)-1));
    }

    query.sort({ 'date':-1 });

    query
    .lean()
    .exec(function(err, list) {
      if (err || pager.no_pagination || !pager.limit) {
        return callback(err, list, null);
      }
      Challenges.count(criteria).lean().exec(function (err, count) {
        if (err) {
          return callback(err, list, null);
        }
        return callback(err, list, utils.get_pagination(pager, count));
      });
    });
  },

  /**
   * Получить список сигранных пользователями игр
   */
  games_list: function (filter, sort, req_data, pager, callback)
  {
    var criteria = {};

    if (filter.user_id || filter.user) {
      criteria.user = filter.user_id || filter.user._id;
    }

    if (filter.challenge_id || filter.challenge) {
      criteria.challenge = filter.challenge_id || filter.challenge._id;
    }

    if (filter.year) {
      var date_start = moment([filter.year, (filter.month || 1)-1, 1, 0, 0, 0, 0]).toDate();
      var date_end  = moment(date_start).add((filter.month ? 'months' : 'years'), 1).toDate();
      criteria.date = { $gte: date_start, $lt: date_end };
    }

    var query = ChallengesAnswers.find(criteria, { __v:0, answers_list:0 });

    if (pager.limit) {
      query.limit(pager.limit).skip(pager.limit * ((pager.page||1)-1));
    }

    if (sort.answers_count) {
      query.sort({ 'answers_count':sort.answers_count });
    }
    else {
      query.sort({ 'sort':-1 });
    }

    if (req_data && req_data.challenge) {
      query.populate('challenge', { __v:0, answers_list:0 })
    }

    if (req_data && req_data.user) {
      query.populate('user', { login:1, avatar:1 })
    }

    query
    .lean()
    .exec(function(err, list) {
      if (err) {
        return callback(err);
      }
      if (sort.answers_count) {
        list = utils.setRatingsPosition(list, 1, 'answers_count');
      }

      if (pager.no_pagination || !pager.limit) {
        return callback(err, list);
      }

      ChallengesAnswers.count(criteria).lean().exec(function (err, count) {
        if (err) {
          return callback(err, list);
        }
        return callback(err, list, utils.get_pagination(pager, count));
      });
    });
  },

  /**
   * Получить детальную информацию по игре
   */
  game: function(filter, req_data, callback)
  {
    var self = challenge_module;
    if (filter.date) {
      Challenges.findOne(
        { date: filter.date }, { __v:0, answers_list:0 })
      .lean()
      .exec(function(err, challenge) {
        if (err || !challenge) {
          return callback(err, null, null, null);
        }
        make_detail(challenge);
      });
    }

    if (filter.challenge) {
      make_detail(filter.challenge);
    }
    
    function make_detail(challenge)
    {
      async.parallel({
        answers: function(callback) {
          if (req_data.user) {
            get_answers(challenge._id, req_data.user, callback)
          }
          else {
            callback(null, null);
          }
        },
        rating: function(callback) {
          if (req_data.rating && challenge.is_active) {
            self.games_list(
              { challenge_id:challenge._id },
              { 'answers_count':-1 },
              { user:true },
              { limit:10, no_pagination:true },
              callback
            );
          }
          else if (req_data.rating && !challenge.is_active) {
            callback(null, ( challenge.results||[] ));
          }
          else {
            callback(null, null);
          }
        }
      }, 
      function(err, results) {
        var result_rating = results.rating || [],
            result_answers = results.answers && results.answers.answers_list || [];
        return callback(err, challenge, result_answers, result_rating);
      });
    }

    function get_answers(challenge_id, user_id, callback)
    {
      ChallengesAnswers.findOne(
        { challenge:challenge_id, user:user_id })
      .lean().exec(callback);
    }
  },

  /**
   * Добавить ответ в игру
   */
  add_game_answer: function(filter, answer, callback) 
  {
    var self = challenge_module,
        user_id = filter.user_id,
        criteria = {};

    if (!user_id) {
      return callback(utils.ValidationError('main', 'invalid_user'));
    }

    if (filter.date) {
      Challenges.findOne({ date:filter.date }).lean().exec(function(err, challenge) {
        if (err || !challenge) {
          return callback(err, null);
        }
        get_answers(challenge);
      });
    }
    if (filter.challenge_id) {
      Challenges.findById(filter.challenge_id).lean().exec(function(err, challenge) {
        if (err || !challenge) {
          return callback(err, null);
        }
        get_answers(challenge);
      });
    }
    if (filter.challenge) {
      get_answers(filter.challenge);
    }

    var get_answers = function(challenge)
    {
      ChallengesAnswers.findOne(
        { challenge: challenge._id, 
          user: user_id },
        function(err, user_answers) {
          if (err) {
            return callback(err);
          }
          add_answer(challenge, user_answers);
        }
      );
    }

    var add_answer = function(challenge, user_answers)
    {
      if (!challenge_utils.is_answer(challenge, answer)) {
        return callback(utils.ValidationError('answer', 'invalid_answer'));
      }
      if (!user_answers) {
        user_answers = new ChallengesAnswers({
          challenge: challenge._id,
          user: user_id,
          sort: challenge.date.getTime(),
          answers_list: [],
          answers_count: 0
        });
      }
      if (challenge_utils.answer_exist(user_answers, answer)) {
        return callback(utils.ValidationError('answer', 'duplicate_answer'));
      }
      challenge_utils.add_answer(user_answers, answer);
      user_answers.save(function(err) {
        if (err) {
          return callback(err);
        }
        self.games_list(
          { challenge_id:challenge._id },
          { 'answers_count':-1 },
          { user:true },
          { limit:10, no_pagination:true },
          function(err, rating) {
            if (err) {
              return callback(err);
            }
            return callback(err, challenge, answer, user_answers, rating);
          }
        );
      });
    }
  },

  /**
   * Добавляем в существующие слова
   * новый возможный ответ
   */
  extend_challenge_answers: function(new_answer, callback)
  {
    Challenges.find({}, { _id:1, word:1 }, function(err, list) 
    {
      if (err) {
        return callback(err);
      }
      var asyncTasks = [];
      list.forEach(function(challenge) {
        if (is_challenge_answer(challenge.word, new_answer)) 
        {
          asyncTasks.push(function(callback) {
            Challenges.findByIdAndUpdate(
              challenge._id, { $push:{ answers_list:new_answer }})
            .lean().exec(function(err) {
              callback(err, true)
            });
          });
        }
      });
      async.parallel(asyncTasks, callback);
    });
  }
}

var challenge_utils = {
  is_answer: function(challenge, word) {
    return ~challenge.answers_list.indexOf(word);
  },
  answer_exist: function(challenge_game, word) {
    return ~challenge_game.answers_list.indexOf(word);
  },
  add_answer: function(challenge, word) {
    challenge.answers_list.push(word);
    challenge.answers_count += 1;
  },
  is_challenge_answer: function(challenge_word, answer) {
    var challenge_word = challenge_word.split('');
    for (var i in answer) {
      var idx = challenge_word.indexOf(answer[i]);
      if (idx === -1) {
        return false;
      }
      delete challenge_word[idx];
    }
    return true;
  }
}

module.exports = challenge_module;