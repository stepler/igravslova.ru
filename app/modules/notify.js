var config = global.config,
    path = require('path');

var _ = require('lodash'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    utils = require(path.join(config.path.lib, 'utils'));

var Notify = mongoose.model('Notify'),
    NotifyGlobal = mongoose.model('NotifyGlobal');

var notify_module = {

  save_message: function(user_id, title, message, callback)
  {
    Notify.create({
      user: user_id,
      title: title,
      message: message
    }, 
    function(err, message) {
      if (err) {
        return callback(err);
      }

      return callback(null, message);
    });
  },

  get_messages: function(user_id, mark_as_read, callback)
  {
    var self = this;

    Notify.find({ user:user_id, is_read:false }, function(err, messages) {
      if (err) {
        return callback(err);
      }

      if (!mark_as_read) {
        return callback(null, messages);
      }

      self.set_read_status(user_id, function(err) {
        if (err) {
          return callback(err);
        }

        callback(null, messages);
      })
    });
  },

  set_read_status: function(user_id, callback)
  {
    Notify.update({ user:user_id, is_read:false }, { is_read:true }, callback);
  },

  get_global_messages: function(date_from, callback)
  {
    NotifyGlobal.find(
      { 'date':{ $gt:date_from.toDate()} }, function(err, messages) {

      if (err) {
        return callback(err);
      }
      callback(null, messages);
    });
  },

  isset_global_messages: function(date_from, callback)
  {
    NotifyGlobal.count(
      { 'date':{ $gt:date_from.toDate() }}, function(err, count) {
      if (err) {
        return callback(err);
      }
      callback(null, (count > 0));
    });
  }
};

module.exports = notify_module;