var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils'));

var Users = mongoose.model('Users'),
    UsersRating = mongoose.model('UsersRating');


var rating_module = {

  /**
   * Вернуть рейтиг определенного пользователя
   */
  rating_user: function(user_id, callback) {
    UsersRating
    .findOne({ user:user_id })
    .populate('user', { login:1, avatar:1 })
    .exec(callback);
  },
 
  /**
   * Вернуть рейтинг пользователей
   */
  rating_list: function(filter, sort, pager, callback)
  {
    var criteria = { rating:{ $gt:0 } },
        query = UsersRating.find(criteria, { __v:0, awards_list:0, words_list:0, achievements:0 });

    if (pager.limit) {
      query.limit(pager.limit).skip(pager.limit * ((pager.page||1)-1));
    }

    query.sort({'rating': -1})

    query
    .populate('user', { login:1, avatar:1 })
    // .where('rating').gt(0)
    .exec(function(err, list) {
      if (err) {
        return callback(err, list);
      }
      if (pager.no_pagination || !pager.limit) {
        list = utils.setRatingsPosition(list, 1, 'rating');
        return callback(err, list);
      }
      UsersRating.count(criteria).exec(function (err, count) {
        if (err) {
          return callback(err, list);
        }
        list = utils.setRatingsPosition(list, (pager.limit * (pager.page-1) + 1), 'rating');
        return callback(err, list, utils.get_pagination(pager, count));
      });
    });
  },

  /**
   * Доавить награду за определенную игру
   */
  add_award: function(position, user, challenge, callback)
  {
    var today = moment({ hour:0, minute:0, second:0, millisecond:0 }),
        awards_map = config.challenges.awards,
        awards_scores = config.challenges.score,
        award = awards_map[position],
        update = { '$inc':{}, '$push':{} };

    update['$inc']['rating'] = awards_scores[position];
    update['$inc']['awards.'+award] = 1;
    update['$push']['awards_list.'+award] = [today.toDate(), challenge.word, awards_scores[position]];
    
    UsersRating.findOneAndUpdate({ user:user._id }, update, callback);
  },

  /**
   * Обновить статистику пользователя
   * по использоваенным словам
   */
  update_word_stat: function(user_id, new_word, callback)
  {
    UsersRating.findOne(
      { user:user_id }, 
      function(err, user_rating) {
        if (err || ~user_rating.words_list.indexOf(new_word)) {
          return callback(err, false);
        }
        user_rating.words_list.push(new_word);
        user_rating.words_count += 1;
        user_rating.save(function(err) {
          return callback(err, user_rating);
        });
      }
    );
  },

  /**
   * Обновить достижения пользователя
   * TODO
   */
  update_achievments: function(user_id, new_word, callback)
  {
  }
};

module.exports = rating_module;