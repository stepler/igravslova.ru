var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    crypto = require('crypto'),
    utils = require(path.join(config.path.lib, 'utils'));

var Users = mongoose.model('Users'),
    UsersRating = mongoose.model('UsersRating');


var user_module = {

  /**
   * Найти пользователя для сессии
   */
  get_user_session: function(user_id, callback)
  {
    Users.findById(user_id, 'id login email provider avatar', function(err, user) {
      callback(err, user);
    });
  },

  /**
   * Найти пользователя 
   */
  get_user: function(user_id, callback)
  {
    Users.findById(user_id, callback);
  },

  /**
   * Авторизовать пользователя
   */
  authenticate: function(email, password, callback)
  {
    Users.findOne({ email:email }, function(err, user) {
      if (err) {
        return callback(err);
      }
      if (!user) {
        return callback(utils.ValidationError('email', 'user_not_found'));
      }
      if (!user.is_correct_password(password)) {
        return callback(utils.ValidationError('password', 'invalid_password'));
      }
      return callback(null, user);
    });
  },

  /**
   * Зарегистрировать пользователя
   * Регистрация производится через oAuth
   * или через стандартную форму логин / пароль
   */
  register: function(login, email, password, provider, profile, callback)
  {
    if (user_utils.is_oauth(provider)) {
      Users.findOne({ 'profile.id':profile.id, provider:provider }, function(err, user) {
        if (err) {
          return callback(err);
        }
        if (user) {
          user.profile = profile;
          user.save();
          return callback(null, user);
        }
        create_user();
      });
    }
    else {
      Users.findOne({ 'email':email }, function(err, user) {
        if (err) {
          return callback(err);
        }
        if (user) {
          return callback(utils.ValidationError('email', 'exist_email'));
        }
        create_user();
      });
    }

    var create_user = function()
    {
      var user = new Users({
        login: login,
        email: email,
        password: password,
        provider: provider,
        email_confirm_token: utils.genToken(),
        // Если есть E-mail - то он не потдвержден
        is_confirmed_email: (email ? false : true),
        avatar: user_utils.getAvatar(email, login),
        profile: profile
      });
      user.save(function(err) {
        if (err) {
          return callback(err);
        } 
        return callback(null, user);
      });
    }
  },

  /**
   * Обновить профиль пользователя
   */
  update_profile: function(user_id, user_data, current_password, callback)
  {
    Users.findById(user_id, function(err, user) {
      if (err) {
        return callback(err);
      }
      if (!user) {
        return callback(utils.ValidationError('main', 'user_not_found'));
      }
      // Если не oAuth профиль, проверяем корректность пароля
      // if (!user_utils.is_oauth(user.provider) && !user.is_correct_password(current_password)) {
      //   return callback(utils.ValidationError('current_password', 'invalid_password'));
      // }

      if (user_data.email) {
        if (user.email != user_data.email) {
          user.email_confirm_token = utils.genToken();
          user.is_confirmed_email = false;
        }
        user.email = user_data.email;
      }

      if (user_data.login) {
        user.login = user_data.login;
      }
      // Если не oAuth профиль, меняем пароль если он задан
      // if (!user_utils.is_oauth(user.provider) && !_.isEmpty(user_data.password)) {
      //   user.password = user_data.password;
      // }
      user.save(function(err) {
        if (err) {
          return callback(err);
        }
        return callback(null, user);
      });
    });
  },

  /**
   * Восстановить пароль:
   * запросить токен восстановления или задать пароль по токену
   */
  restore_password: function(emain_or_token, password, callback)
  {
    var email = token = emain_or_token;
    if (_.isEmpty(password))
    {
      Users.findOneAndUpdate(
        { email:email, provider:'local' },
        { $set:{ restore_token:utils.genToken() } },
        function(err, user) {
          if (err) {
            return callback(err);
          }
          if (!user) {
            return callback(utils.ValidationError('email', 'user_not_found'));
          }
          return callback(null, user);
        }
      );
    }
    else 
    {
      Users.findOne({ restore_token:token }, function(err, user) {
        if (err) {
          return callback(err);
        }
        if (!user) {
          return callback(utils.ValidationError('restore_token', 'invalid_token'));
        }
        user.restore_token = undefined;
        user.password = password;
        user.save(function(err) {
          if (err) {
            return callback(err);
          }
          return callback(null, user);
        });
      });
    }
  },

  /**
   * Подтвердить email по токену
   * @return {[type]} [description]
   */
  confirm_email: function(token, callback)
  {
    Users.findOne({ email_confirm_token:token }, function(err, user) {
      if (err) {
        return callback(err);
      }
      if (!user) {
        return callback(utils.ValidationError('confirm_token', 'invalid_token'));
      }
      user.email_confirm_token = undefined;
      user.is_confirmed_email = true;
      user.save(function(err) {
        return callback(null, user);
      });
    });
  }
};

var user_utils = {

  is_oauth: function(provider)
  {
    return !!~['yandex', 'vkontakte', 'facebook', 'google'].indexOf(provider);
  },

  getAvatar: function(email, login)
  {
    return crypto.createHmac('sha1', 'email').update((email || login)).digest('hex');
  },

  setConfirmEmailToken: function()
  {
    this.is_confirmed_email = false;
    this.email_confirm_token = utils.genToken();
  },

  clearConfirmEmailToken: function()
  {
    this.is_confirmed_email = true;
    this.email_confirm_token = null;
  }
};

module.exports = user_module;