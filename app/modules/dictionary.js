var config = global.config,
    path = require('path');

var _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    async = require('async'),
    utils = require(path.join(config.path.lib, 'utils')),
    challenge_module = require(path.join(config.path.modules, 'challenge'));

var Dictionary = mongoose.model('Dictionary'),
    DictionaryUserword = mongoose.model('DictionaryUserword');

var dictionary_module = {

  /**
   * Найти следующую игру
   */
  find_next_challenge: function(callback) 
  {
    var self = this,
        word_length = _.sample(config.challenges.words_length);

    Dictionary.findOne({ is_next:true }, function(err, word) {
      if (err) {
        return callback(err, null);
      }
      if (word) {
        return callback(null, null);
      }

      var conditions = { is_use:false, 
        length:word_length, word:{ $not:new RegExp('(ость|ство)$') } };

      Dictionary.count(conditions, function(err, count) {
          if (err) {
            return callback(err, null);
          }

          var rand = Math.floor(Math.random() * count);
          Dictionary.findOne(conditions, null, { skip:rand }, function(err, word) {
              if (err || !word) {
                return callback(err, null);
              }
              dictionary_utils.gen_answers(word.word, function(err, answers) {
                if (err) {
                  return callback(err, null);
                }
                word.answers_list = answers;
                word.is_next = true;
                word.save(callback);
              });
            }
          );
        }
      );
    });

  },

  /**
   * Добавить новую игру
   */
  add_challenge: function(callback) 
  {
    Dictionary.findOne({ is_next:true }, function(err, dict_word) {
      if (err || !dict_word) {
        return callback(err, dict_word);
      }
      challenge_module.add_new(dict_word.word, dict_word.answers_list, function(err, challenge) {
        if (err) {
          return callback(err);
        }
        dict_word.is_use = true;
        dict_word.is_next = false;
        dict_word.save(function(err, dict_word) {
          return callback(err, challenge, dict_word);
        });
      })
    });
  },

  /**
   * Добавить в словарь новое слово
   * слово добавляется пользователем (user_id)
   */
  extend_dict: function(word, user_id, callback)
  {
    Dictionary.findOne({ word:word }, function(err, find_word) {
      if (err) {
        return callback(err);
      }
      if (find_word) {
        return callback(utils.ValidationError('main', 'duplicate_word'));
      }
      var word_index = dictionary_utils.gen_index(word);
      Dictionary.create({
          word: word,
          index: word_index,
          length: word.length,
          type: 'user-word',
          type_raw: user_id
        }, 
        function(err, new_word) {
          if (err) {
            return callback(err);
          }
          return callback(null, new_word);
        }
      );
    });
  },

  /**
   * Добавить слово пользователя в таблицу запросов
   */
  add_userword: function(word, user, challenge, callback)
  {
    DictionaryUserword.create({
        word: word,
        user_login: user.login,
        user: user._id,
        challenge: challenge._id,
        token: utils.genToken()
      }, 
      function(err, new_userword) {
        if (err) {
          return callback(err);
        }
        return callback(null, new_userword);
      }
    );
  }
};

var dictionary_utils = {

  gen_answers: function(word, callback) 
  {
    var self = this,
        answers = [],
        // word = word_doc.word,
        word_index = word.split('').sort().join('');

    var counter = Math.pow(2, word.length+1),
        answer_index = '';

    counter--; // убираем нахождение самого слова
    _build_answers(null, null);

    function _build_answers(err, result) {
      if (err) {
        return callback(err, null);
      }

      if (result) {
        for (var i in result) {
          var item = result[i];
          if (answers.indexOf(item.word) == -1) {
            answers.push(item.word);
          }
        }
      }

      counter--;
      var mask = counter.toString(2).substr(1);
      answer_index = '';
      for (var i in mask) {
        if (parseInt(mask[i])) {
          answer_index += word_index[i];
        }
      }

      if (answer_index.length > 1) {
        Dictionary.find({ index:answer_index }).exec(_build_answers)
      }
      else if (answer_index.length > 0) {
        _build_answers(null, null);
      }
      else {
        return callback(null, answers);
      }
    }
  },

  gen_index: function(word)
  {
    return word.replace(/[^\u0410-\u044F]/gi, '').split('').sort().join('');
  }
};

module.exports = dictionary_module;