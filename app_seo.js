var path = require('path'),
    http = require('http'),
    url = require('url'),
    childProcess = require('child_process');

var config = require('./config/config')[(process.env.NODE_ENV || 'development')];

var binPathPhantomjs = '/usr/lib/node_modules/phantomjs/lib/phantom/bin/phantomjs';

var server = http.createServer(function (req, res) 
{
  var query_params, request_url, route;
  query_params = url.parse(req.url, true).query;
  route = query_params._escaped_fragment_;
  request_url = 'http://127.0.0.1:'+config.app.port+'/#!' + decodeURIComponent(route);
  
  var childArgs = [
    '--disk-cache=no',
    path.join(__dirname, 'seo', 'angular-seo.js'), 
    request_url
  ];
  childProcess.execFile(binPathPhantomjs, childArgs, function(err, stdout, stderr) {
    res.end(stdout);
  });
});

server.listen(config.app.seo_port);
