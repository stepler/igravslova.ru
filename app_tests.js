var path = require('path');
global.config = require('./config/config')['tests'];

var config = global.config;
require('./config/db')(config);
global.schema = require(path.join(config.path.tests, 'json_schema'));

require(path.join(config.path.conf, 'i18n'))(config);

module.exports = {
  init: require(path.join(config.path.tests, 'init'))(),
  challenges_api: require(path.join(config.path.tests, 'challenges_api'))(),
  tasks: require(path.join(config.path.tests, 'tasks'))(),
  // achievements_config: require(path.join(config.path.tests, 'achievements_config'))(),
  // dictionary_api: require(path.join(config.path.tests, 'dictionary_api'))(),
  // redis_cache: require(path.join(config.path.tests, 'redis_cache'))(),
}