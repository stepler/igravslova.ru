'use strict';

angular
.module('app.notify', ['ui.bootstrap'])

.service('ws_notify', ['$rootScope', '$location', '$timeout', '$modal', 
  function($rootScope, $location, $timeout, $modal) {

    function open(title, message) {
      $modal.open({
        template: (function(){ return $('#notifier-tpl').html() })(),
        controller: NotifyModalCtrl,
        resolve: {
          title: function () { return title; },
          message: function () { return message; }
        }
      });
    }

    var NotifyModalCtrl = function($scope, $modalInstance, title, message) {
      var timer = null;
      $scope.title = title;
      $scope.message = message;
      // element.on('mouseover', function(){
      //   if (timer) {
      //     $timeout.cancel(timer);
      //   }
      // });

      // element.on('mouseout', function(){
      //   timer = $timeout(hideMessage, 2000);
      // });

      $scope.close = function () { $modalInstance.close('close'); };
    }

    return {
      init: function() {
        if (!$rootScope.isAuth) {
          return ;
        }
        var socket = io.connect($location.absUrl());
        socket.on('notify', function(data) {
          open(data.title, data.message);
        });
      }
    }
  }
]);
