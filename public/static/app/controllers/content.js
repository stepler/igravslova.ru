'use strict';

angular
.module('app.controllers.content', 
    [])

.controller('RulesCtrl', ['$scope', '$http', '$sce',
  function($scope, $http, $sce) {
    $scope.title = null;
    $http
    .get('/content/rules')
    .success(function(data) {
      $scope.set_title('Правила игры');
      $scope.title = $sce.trustAsHtml(data.title);
      $scope.text = $sce.trustAsHtml(data.text);

      $scope.htmlReady();
    });
  }
])

.controller('FeedbackCtrl', ['$scope', '$stateParams', '$http', '$app_forms', '$system_modals', '$state', 
  function($scope, $stateParams, $http, $app_forms, $system_modals, $state) {
    $scope.set_title('Обратная связь');
    $scope.token = $stateParams.token;
    function reset_form(){
      $scope.form_errors = {};
      $scope.form = { _csrf:CSRF_TOKEN, feedback_subject:'' };
      if ($scope.token) {
        $scope.form.token = $scope.token;
      }
    }
    reset_form();
    $scope.submit = function() {
      $http
      .post('/feedback', $scope.form)
      .success($app_forms.submit_success(function(data) {
        reset_form();
        $system_modals.success({
          title: 'Готово! Письмо уже летит к нам.',
          content: 'Совсем скоро мы с ним ознакомимся, и если оно требует ответа, то&nbsp;свяжемся с вами.'
        });
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

