'use strict';

angular
.module('app.controllers.challenges', 
    ['app_utilites', 'app_filters', 'ui.bootstrap'])

.controller('MainCtrl', ['$scope', '$http', 'challengeWordResize',
  function($scope, $http, challengeWordResize) {
    var readyCounter = 0;
    $scope.set_title('Словолом &mdash; игра в слова', true);

    $http
    .get('/api/challenges/?limit=11&page=0')
    .success(function(data) {
      if (data.challenges.length) {
        $scope.challenge_of_day = data.challenges.shift();
        $scope.challenges_list = data.challenges;
        
        challengeWordResize($scope.challenge_of_day.word.length, true);
        setReady();
      }
    });
    $http
    .get('/api/rating/?limit=10&page=0')
    .success(function(data) {
      $scope.rating_list = data.rating || [];
      setReady();
    });

    function setReady() {
      if (readyCounter++ && readyCounter >=2) {
        $scope.htmlReady();
      }
    }
  }
])

.controller('ChallengeCtrl', ['$scope', '$stateParams', '$http', '$modal', '$app_forms', 'challengeWordResize',
  function($scope, $stateParams, $http, $modal, $app_forms, challengeWordResize) {
    var answers_list = [],
        answers_view_count = 10,
        answers_view_page = 1,
        date = $stateParams.date;
    $scope.answer = '';
    $scope.input_error = '';
    $scope.answers_paging = false;
    $scope.answers_list = [];
    $scope.need_auth = _.isNull(USER);

    $http
    .get('/api/challenges/'+date)
    .success(function(data) {
      $scope.set_title('Игра '+date+' &laquo;'+data.challenge.word+'&raquo;');

      $scope.challenge = data.challenge;
      answers_list = (data.answers || []).reverse();
      $scope.answers_list = getViewList();
      $scope.answers_count = answers_list.length;
      $scope.rating_list = data.rating;
      $scope.current_time = data.current_time;

      challengeWordResize($scope.challenge.word.length);

      $scope.htmlReady();
    });

    $scope.addAnswer = function() {
      var answer = $scope.answer;
      $http
      .put('/api/challenges/'+date, { answer:answer, _csrf:CSRF_TOKEN })
      .success(function(data) {
        answers_list.unshift(answer);
        $scope.answers_list = getViewList();
        $scope.answers_count = answers_list.length;
        if (data.rating) {
          $scope.rating_list = data.rating;
        }
        $scope.$broadcast('game.answer-added');
        $scope.input_error = ['Слово добавлено', 0];
      })
      .error(function(data) {
        if (data[0]) {
          var err = data[0];
          $scope.input_error = [(err.message || err.code), 2];
        }
      });
    }

    function getViewList() {
      if (answers_view_page == 0) {
        return answers_list;
      }
      $scope.answers_paging =  (answers_view_page*answers_view_count < answers_list.length);
      return answers_list.slice(0, answers_view_page*answers_view_count);
    }

    $scope.show = function(inc) {
      if (inc == 1) {
        answers_view_page += 1;
        if (answers_view_page*answers_view_count >= answers_list.length) {
          answers_view_page = 0;
        }
      }
      else {
        answers_view_page = 0;
      }
      $scope.answers_paging = answers_view_page > 0;
      $scope.answers_list = getViewList();
    }

    $scope.getTimerInfo = function() {
      $modal.open({
        template: (function(){ return $('#challenge-modal-timer-tpl').html() })(),
        controller: function($scope, $modalInstance){
          $scope.close = function () { $modalInstance.close('close'); };
        }
      });
    }

    $scope.setAnswer = function(answer) {
      $scope.answer = answer;
    }

    $scope.sendAnswer = function() {
      $modal.open({
        template: (function(){ return $('#challenge-modal-extend-tpl').html() })(),
        controller: extendDictCtrl,
        resolve: {
          answer: function () { return $scope.answer; },
          challenge: function () { return $scope.challenge; }
        }
      });
    }

    var extendDictCtrl = function($scope, $modalInstance, answer) {
      $scope.data = { _csrf:CSRF_TOKEN, ext_word:answer }
      $scope.form_submit = null;
      $scope.form_errors = {};

      $scope.close = function () { $modalInstance.close('close'); };
      $scope.send = function () {
        $http
        .post('/api/challenges/'+date+'/extend', $scope.data)
        .success($app_forms.submit_success(function(data) {
          $scope.form_errors = {};
          $scope.form_submit = 'success';
        }))
        .error($app_forms.submit_error($scope.form_errors, function(data) {
          $scope.form_submit = 'fail';
        }));
      }
    }
  }
])
.directive('gameSetAnswer', [function() {
  return {
    restrict: 'E',
    replace: true,
    template: 
      '<div>' +
        '<span class="b-game__input" contenteditable="true"></span>' +
        '<b class="b-game__mask-item" ng-class="item_mask_class"></b>' +
        '<a href="" class="b-game__clear-btn" title="Очистить" ng-click="clear()" ng-class="btn_clear_class">' +
          '<i class="icon-cancel"></i>' +
        '</a>' +
      '</div>',
    link: function(scope, element, attrs) {
      var $scope = scope.$parent;
      var $word = $('#game-word'),
          $letterLinks = [], 
          $input = element.find('span'),
          _input = $input[0];

      var ENTER_CODE = 13;

      var InputCtrl = {
        text: [],
        input_symbols: [],
        symbols_nodes: [],
        symbols_available: [],
        symbols_exist: [],

        add: function(symbol, $node) {
          this.text.push([symbol, $node, $node.get(0)]);
          this.render();
        },
        rem: function(symbol, $node) {
          var node = $node.get(0);
          for (var i in this.text) {
            if (this.text[i][2] == node) {
              this.text.splice(i, 1);
            }
          }
          this.render();
        },
        set: function(text) {
          if (text.length > this.text.length) {
            // add
            for (var i in text) {
              if (!this.text[i] || text[i] != this.text[i][0]) {
                var nodes = this._getFirstSymbol(text[i]);
                if (nodes) {
                  this.text.push([text[i], nodes[1], nodes[2]]);
                }
                // this.text.splice(i, 0, [text[i], nodes[1], nodes[2]]);
              }
            }
          }
          else {
            // rem
            for (var i in text) {
              if (!this.text[i] || text[i] != this.text[i][0]) {
                this.text.splice(i, 1);
              }
            }
            if (text.length < this.text.length) {
              this.text.splice(text.length, (this.text.length-text.length));
            }
          }
          this.render(true);
        },
        clear: function() {
          this.text = [];
          this.render();
        },
        resetNodes: function()
        {
          this.text = [];
          this.input_symbols = [];
          this.symbols_nodes = [];
          this.symbols_available = [];
          this.symbols_exist = [];
        },
        setNode: function(symbol, $node) {
          this.symbols_nodes.push([symbol, $node, $node.get(0)]);
          this.symbols_available.push(symbol);
          this.symbols_exist.push(symbol);
        },
        getInputWord: function() {
          return this.text.join('');
        },
        _getFirstSymbol: function(symbol) {
          for (var i in this.symbols_nodes) {
            if (symbol == this.symbols_nodes[i][0] && !this.symbols_nodes[i][1].hasClass('disable')) {
              return this.symbols_nodes[i];
            }
          }
          return null;
        },
        _update: function() {
          var text_arr = this.text.slice(0),
              found = false;
          this.symbols_available = [];
          this.input_symbols = [];
          for (var i in this.symbols_nodes) {
            found = false;
            for (var j in text_arr) {
              if (this.symbols_nodes[i][2] === text_arr[j][2]) {
                text_arr.splice(j, 1);
                found = true;
              }
            }
            this.symbols_nodes[i][1][(found?'addClass':'removeClass')]('disable');
            // this.symbols_nodes[i][1].toggleClass('disable', found);
            if (!found) {
              this.symbols_available.push(this.symbols_nodes[i][0]);
            }
          }
          for (var i in this.text) {
            this.input_symbols.push(this.text[i][0]);
          }
        },
        isAvailable: function(symbol) {
          return this.symbols_available.indexOf(symbol) >= 0;
        },
        isContain: function(symbol) {
          return this.symbols_exist.indexOf(symbol) >= 0;
        },
        render: function(set_caret) {
          var self = this;
          this._update();
          $input.html('<b>'+(this.input_symbols.join('</b><b>'))+'</b>');
          scope.$apply(function(){
            self._renderClearBtn(self.input_symbols.length, self.symbols_available.length);
          });
          $scope.$apply(function(){
            $scope.setAnswer(self.input_symbols.join(''));
          });
          if (set_caret) {
            placeCaretAtEnd(_input);
          }
        },
        _renderClearBtn: function(input_length, available_length) {
          if (!input_length || !available_length) {
            scope.item_mask_class = '';
            scope.btn_clear_class = '';
          }
          else {
            scope.item_mask_class = 'b-game__mask-item_hide';
            scope.btn_clear_class = 'b-game__clear-btn_show';
          }
        }
      };

      setTimeout(init, 100);
      // init();

      scope.clear = function() {
        setTimeout(function(){ InputCtrl.clear() }, 0);
      }

      scope.$on('game.answer-added', function() {
        scope.clear();
      });

      function init()
      {
        var word = $scope.challenge.word;
        if (!word) {
          return;
        }
        $input.after(new Array(word.length).join('<b></b>'));
        
        InputCtrl.resetNodes();
        $letterLinks = $word.children();
        $input.off('keypress input');
        $letterLinks.off('click');
        $input.html('<b></b>');

        $letterLinks.each(function(i){
          InputCtrl.setNode(word[i].toLowerCase(), $(this));
        });

        $input.on('keypress', function(e) {
          if (e.keyCode == ENTER_CODE) {
            $scope.addAnswer();
            return;
          }
          var S = String.fromCharCode(e.charCode).toLowerCase(),
              availableSymbol = InputCtrl.isAvailable(S),
              isContain = InputCtrl.isContain(S);

          if (!availableSymbol) {
            $scope.$apply(function(){
              if (isContain) {
                $scope.input_error = ['В слове не осталось свободного символа <strong>&laquo;'+(S.toUpperCase())+'&raquo;</strong>', 1];
              }
              else {
                $scope.input_error = ['В слове нет символа <strong>&laquo;'+(S.toUpperCase())+'&raquo;</strong>', 1];
              }
            });
          }
          return availableSymbol;
        });

        $input.on('input', function(e) {
          InputCtrl.set($input.text().toLowerCase());
        });

        $letterLinks.on('click', function(e) {
          var $link = $(this),
              letter = $link.text().toLowerCase();
          if ($link.hasClass('disable')) {
            InputCtrl.rem(letter, $link);
          }
          else {
            InputCtrl.add(letter, $link);
          }
        });
      }
      
      function placeCaretAtEnd(el) {
        el.focus();
        if (window.getSelection && document.createRange) {
          var range = document.createRange();
          range.selectNodeContents(el);
          range.collapse(false);
          var sel = window.getSelection();
          sel.removeAllRanges();
          sel.addRange(range);
        }
        else if (document.body.createTextRange) {
          var textRange = document.body.createTextRange();
          textRange.moveToElementText(el);
          textRange.collapse(false);
          textRange.select();
        }
      }
    }
  };
}])
.directive('gameTimer', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    scope: {
      isActive: '='
    },
    template: function(){ return $('#challenge-timer-tpl').html() },
    link: function(scope, element, attrs) {
      var currTime = moment(parseInt(attrs.nowTime)),
          endTime = null;

      $timeout(function(){ launchTimer() });

      function launchTimer()
      {
        endTime = moment(parseInt(attrs.endTime));
        var days = endTime.diff(currTime, 'days');
        if (days > 0) {
          var hours = endTime.subtract(days, 'd').diff(currTime, 'hours');
          setTimer(
            days, ['день','дня','дней'], 
            hours, ['час','часа','часов'],
            endTime.subtract(hours, 'h').diff(currTime, 'seconds'), 3600);
          return;
        }

        var hours = endTime.diff(currTime, 'hours');
        if (hours > 0) {
          var minutes = endTime.subtract(hours, 'h').diff(currTime, 'minutes');
          setTimer(
            hours, ['час','часа','часов'], 
            minutes, ['минута','минуты','минут'],
            endTime.subtract(minutes, 'm').diff(currTime, 'seconds'), 60);
          return;
        }

        var minutes = endTime.diff(currTime, 'minutes');
        var seconds = endTime.subtract(minutes, 'm').diff(currTime, 'seconds');
        if (seconds > 0) {
          setTimer(
            minutes, ['минута','минуты','минут'], 
            seconds, ['секунда','секунды','секунд'],
            1, 1);
        }
        else {
          scope.$apply(function() {
            scope.isActive = false;
          });
        }
      }

      function declOfNum(number, titles) 
      {  
        var cases = [2, 0, 1, 1, 1, 2];  
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
      }

      function setTimer(cell1, labels1, cell2, labels2, timeout, interval) 
      {
        if (cell2 < 0) {
          // currTime.add('s', 1);
          return launchTimer();
        }

        currTime.add(timeout, 's');
        scope.$apply(function() {
          scope.cell_1 = cell1 < 10 ? '0'+cell1.toString() : cell1;
          scope.cell_2 = cell2 < 10 ? '0'+cell2.toString() : cell2;
          scope.label_1 = labels1[2], //declOfNum(cell1, labels1);
          scope.label_2 = labels2[2] //declOfNum(cell2, labels2);
        });
  
        if (timeout > 0) {
          $timeout(
            function(){ setTimer(cell1, labels1, cell2-1, labels2, interval, interval) }, 
            timeout*1000
          );
        }
      }
    }
  };
}])
.directive('gameStatMessage', ['$timeout', '$sce',
  function($timeout, $sce) {
    return {
      restrict: 'E',
      require: '^ngModel',
      replace: true,
      template: 
        '<span class="b-game__message">' +
          '<span class="b-game__message-text" ng-bind-html="message"></span>' +
        '</span>',
      link: function(scope, element, attrs, ngModel) 
      {
        var timer = null;
        var types = ['success', 'warning', 'error'];

        element.on('mouseover', function(){
          if (timer) {
            $timeout.cancel(timer);
          }
        });

        element.on('mouseout', function(){
          timer = $timeout(hideMessage, 2000);
        });

        element.on('click', function(e) {
          switch($(e.target).data('type')) {
            case 'send-answer' : scope.sendAnswer(); break;
          }
        });

        ngModel.$render = function() {
          if (ngModel.$viewValue) {
            var msgType = types[ngModel.$viewValue[1]] || 'warning';

            $timeout.cancel(timer);
            element.removeClass(types.join(' '));
            element.addClass('show '+msgType);
            scope.message = $sce.trustAsHtml(ngModel.$viewValue[0] || '');
            element.triggerHandler('mouseout');
            // timer = $timeout(hideMessage, 2000);
          }
        };

        function hideMessage() {
          element.removeClass('show');
        }
      }
    };
  }
])

.controller('ChallengesCtrl', ['$scope', '$state', '$stateParams', '$http', 
  function($scope, $state, $stateParams, $http) 
  {
    $scope.set_title('Список слов');
    var launchDate = new Date(2014, 1),
        nowDate = new Date();

    var months = [ 
          ['Январь', '01'], ['Февраль', '02'], ['Март', '03'], 
          ['Апрель', '04'], ['Май', '05'], ['Июнь', '06'], 
          ['Июль', '07'], ['Август', '08'], ['Сентябрь', '09'], 
          ['Октябрь', '10'], ['Ноябрь', '11'], ['Декабрь', '12']
        ];

    $scope.filter = {
      years: _.range(nowDate.getFullYear(), launchDate.getFullYear()-1, -1),
      months: []
    };
    $scope.active = {
      year: false,
      month: false,
      month_name: null
    };
    $scope.challenges_list = [];
    $scope.loading = false;

    $scope.$watch('active.year', function(year) {
      $scope.filter.months = months;
      if (year == nowDate.getFullYear()) {
        $scope.filter.months = $scope.filter.months.slice(0, nowDate.getMonth()+1);
      }
      if (year == launchDate.getFullYear()) {
        $scope.filter.months = $scope.filter.months.slice(launchDate.getMonth());
      }
    });

    $scope.setYear = function(year) {
      $scope.active.year = year;
    }
    $scope.setMonth = function(month) {
      $scope.active.month = month[1];
      $scope.active.month_name = month[0];
      $scope.loading = true;

      $http
      .get(
        '/api/challenges/', 
        { params:{ year:$scope.active.year, month:$scope.active.month, limit:0 } }
      )
      .success(function(data) {
        $scope.loading = false;
        $scope.challenges_list = data.challenges || [];
      });
    }
    $scope.setYear(nowDate.getFullYear());
    $scope.setMonth(months[nowDate.getMonth()]);
  }
]);