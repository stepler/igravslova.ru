'use strict';

angular
.module('app.controllers.notify', ['ui.router', 'ui.bootstrap'])

.controller('NotifyCtrl', ['$scope', '$rootScope', '$location', '$http', '$modal', '$sce',
  function($scope, $rootScope, $location, $http, $modal, $sce) {
    var socket_url, socket,
        isAuth = $rootScope.isAuth;
    $scope.unread_count = 0;
    $scope.messages = [];

    if (isAuth) {
      socket_url = "%prot%://%host%:%port%"
                    .replace('%prot%', $location.protocol())
                    .replace('%host%', $location.host())
                    .replace('%port%', APP_PORT);
      socket = io.connect(socket_url);
      socket.on(PUBSUB_NOTIFY, function(data) {
        set_messages([data]);
      });
    }

    if ($rootScope.have_global_messages) {
      $http
      .get('/notify/global-messages')
      .success(function(data) {
        if (data && data.messages) {
          set_messages(data.messages);
        }
      });
    }

    function set_messages(data)
    {
      var before_len = $scope.messages.length,
          message_length = before_len + data.length;

      if (message_length <= 0) {
        return;
      }

      _.forEach(data, function(item) {
        $scope.messages.push({
          title: $sce.trustAsHtml(item.t),
          message: $sce.trustAsHtml(item.m),
          date: item.d || null
        });
      });
      popupNotify.show(before_len, message_length, $scope.show_messages);
      // $scope.$apply(function() {
      $scope.unread_count = message_length;
      // });
    }

    $scope.show_messages = function() {
      $modal
      .open({
        template: (function(){ return $('#notifier-tpl').html() })(),
        controller: NotifyModalCtrl,
        resolve: {
          messages: function () { return $scope.messages; }
        }
      })
      .result.then(
        function() { $scope.reset_messages() },
        function() {}
      );
    }

    $scope.reset_messages = function() {
      $scope.messages = [];
      $scope.unread_count = 0;
    }

    var NotifyModalCtrl = function($scope, $modalInstance, messages) {
      $scope.messages = messages;
      $scope.close = function () { 
        $modalInstance.close('close'); 
      };
    }

    var popupNotify = {
      $popup: null,
      timer: null,
      tpl: function(count) { 
        return '' +
          '<div class="b-notify__informer-popup">' +
            '<div class="b-page__conteiner">' +
              '<a href="javascript:void(0)" class="b-notify__informer-popup-link">' +
                '<i class="icon-bell"></i> '+ count +
              '</a>' +
            '</div>' +
          '</div>';
      },
      show: function(before, length, on_click) {
        if (before == 0) {
          this.create(length, on_click);
        }
        else if (this.$popup) {
          this.update(length);
        }
      },
      create: function(length, on_click) {
        var self = this;
        this.$popup = $(this.tpl(length)).prependTo('body');
        this.$popup
        .find('a')
        .on('click', function() { self.remove(true); on_click(); })
        .on('mouseover', function(){ clearTimeout(self.timer); })
        .on('mouseout', function(){ self.remove(); });
        this.$popup.animate({ top:'0px' }, 250 );
        
        this.remove();
      },
      update: function(length) {
        this.$popup.find('span').text(length);
        this.remove();
      },
      remove: function(wo_timeout) {
        if (!wo_timeout && !isAuth) {
          return;
        }
        var self = this;
        clearTimeout(this.timer);
        this.timer = setTimeout(
          function(){ 
            if (self.$popup) {
              self.$popup.animate({ top:'-50px' }, 250, 
                function(){ self.$popup.remove(); self.$popup = null; });
            }
          }, 
          wo_timeout ? 0 : 5000
        );
      }
    }
  }
])
;