'use strict';

angular
.module('app.controllers.users', ['ui.router', 'app_utilites'])

.controller('UsersCtrl', ['$scope', '$state', '$http',
  function($scope, $state, $http) {
    if (!_.isNull(USER) && !$state.is('users.request_email')) {
      $state.go('profile.awards');
    }
  }
])

.controller('UsersLoginCtrl', ['$scope', '$http', '$app_forms',
  function($scope, $http, $app_forms, $page_type) {
    $scope.set_title('Авторизация');
    $scope.user = { _csrf:CSRF_TOKEN };
    $scope.form_errors = {};
    $scope.submit = function() {
      $http
      .post('/users/login', $scope.user)
      .success($app_forms.submit_success(function(data) {
        $scope.form_errors = {};
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

.controller('UsersRegisterCtrl', ['$scope', '$http', '$app_forms',
  function($scope, $http, $app_forms) {
    $scope.set_title('Регистрация');
    $scope.user = { _csrf:CSRF_TOKEN };
    $scope.form_errors = {};
    $scope.submit = function() {
      $http
      .post('/api/users', $scope.user)
      .success($app_forms.submit_success(function(data) {
        $scope.form_errors = {};
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

.controller('UsersRequestEmailCtrl', ['$scope', '$http', '$app_forms',
  function($scope, $http, $app_forms) {
    $scope.user = { _csrf:CSRF_TOKEN };
    $scope.form_errors = {};
    $scope.submit = function() {
      $http
      .post('/api/users/request-email', $scope.user)
      .success($app_forms.submit_success(function(data) {
        $scope.form_errors = {};
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

.controller('UsersRestoreCtrl', ['$scope', '$stateParams', '$http', '$app_forms', '$system_modals', '$state', 
  function($scope, $stateParams, $http, $app_forms, $system_modals, $state) {
    $scope.set_title('Восстановить пароль');
    $scope.token = $stateParams.token;
    function reset_form(){
      $scope.form_errors = {};
      $scope.user = { _csrf:CSRF_TOKEN };
      if ($scope.token) {
        $scope.user.token = $scope.token;
      }
    }
    reset_form();
    $scope.submit_request = function() {
      $http
      .put('/api/users/restore', $scope.user)
      .success($app_forms.submit_success(function(data) {
        reset_form();
        $system_modals.success({
          title: 'Что дальше?',
          content: 'Вам на почту выслано письмо, которое содержит инструкции для восстановления пароля.'
        });
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
    $scope.submit_restore = function() {
      $http
      .post('/api/users/restore', $scope.user)
      .success($app_forms.submit_success(function(data) {
        reset_form();
        $system_modals.success(
          {
            title: 'Ура, получилось!',
            content: 'Новый пароль успешно сохранен.<br />С возвращением в Словолом =)'
          }, 
          function($modalInstance) { 
            $modalInstance.close('close');
            $state.go('users.login');
          }
        );
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

.controller('ProfileCtrl', ['$scope', '$state', '$http',
  function($scope, $state, $http) {
    if (_.isNull(USER)) {
      $state.go('users.login');
    }
  }
])

.controller('ProfileAwardsCtrl', ['$scope', '$http',
  function($scope, $http) {
    $scope.set_title('Мои награды');
    $http.get('/api/rating/me').success(function(data) {
      $scope.profile = data.user;
      $scope.rating = {
        list: ['g', 's', 'b'],
        titles: { g:'Золото', s:'Серебро', b:'Бронза' },
        class: { g:'gold', s:'silver', b:'bronze' }
      };
    });
  }
])

.controller('ProfileGamesCtrl', ['$scope', '$http',
  function($scope, $http) {
    $scope.set_title('Мои игры');
    $http
    .get('/api/challenges/answers/me?limit=20')
    .success(function(data) {
      $scope.games_list = data.challenges || [] ;
      $scope.pagination = data.pagination;
    });

    $scope.getPage = function(page) {
      $http
      .get('/api/challenges/answers/me?limit=20&page='+$scope.pagination.page)
      .success(function(data) {      
        $scope.games_list = data.challenges || [] ;
      });
    }
  }
])

.controller('ProfileSettingsCtrl', ['$scope', '$http', '$app_forms', '$system_modals',
  function($scope, $http, $app_forms, $system_modals) {
    $scope.set_title('Мой профиль');
    function reset_form(){
      $scope.form_errors = {};
      $scope.user = USER || {}; 
      $scope.user._csrf = CSRF_TOKEN;
    }
    reset_form();
    $scope.submit = function() {
      $http
      .put('/api/users/me', $scope.user)
      .success($app_forms.submit_success(function(data) {
        reset_form();
        $system_modals.success({
          content: 'Профиль успешно сохранен'
        });
      }))
      .error($app_forms.submit_error($scope.form_errors));
    }
  }
])

.controller('RatingCtrl', ['$scope', '$http',
  function($scope, $http) {
    $scope.set_title('Рейтинг пользователей');
    $http
    .get('/api/rating?limit=20')
    .success(function(data) {      
      $scope.rating_list = data.rating || [] ;
      $scope.pagination = data.pagination;

      $scope.htmlReady();
    });

    $scope.getPage = function() {
      $http
      .get('/api/rating?limit=20&page='+$scope.pagination.page)
      .success(function(data) {      
        $scope.rating_list = data.rating || [] ;
      });
    }
  }
  /*
  function($scope, $http) {
    $scope.is_load = null;
    $scope.rating_list = null;
    $scope.total_items = 33;
    $scope.current_page = 1;
    $scope.loading = false;

    $scope.change_page = function() {
      $scope.loading = true;
      $http
      .get('/api/rating', { params:{ page:$scope.current_page } })
      .success(function(data) {
        if (!$scope.is_load) {
          $scope.is_load = true;
          $scope.total_items = data.pagination.items;
        }
        $scope.rating_list = data.rating || [];
        $scope.loading = false;
      });
    };
    $scope.change_page();
  }
  */
]);