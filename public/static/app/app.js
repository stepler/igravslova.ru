var igavslovaApp = angular.module('igavslovaApp', [
  'ui.router',
  'ngAnimate',
  'seo',
  'app.controllers.challenges',
  'app.controllers.users',
  'app.controllers.notify',
  'app.controllers.content'
])
.config(['$httpProvider', '$locationProvider',
  function($httpProvider, $locationProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    // $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  }
])
.run(['$rootScope', '$location', '$sce',
  function($rootScope, $location, $sce) {
    $rootScope.isAuth = USER !== null;
    $rootScope.user = USER;
    $rootScope.highlight_page = false;
    $rootScope.have_global_messages = HAVE_MESSAGES;

    $rootScope.title = '';
    $rootScope.description = '';

    $rootScope.set_title = function(value, skip_prefix) {
      var prefix = skip_prefix ? '' : 'Словолом &mdash; ';
      $rootScope.title = $sce.trustAsHtml(prefix+value);
    }
    $rootScope.set_description = function(value, skip_postfix) {
      $rootScope.description = $sce.trustAsHtml(value);
    }

    var firstLoad = true;
    var highlight_states = ['index', 'challenge'];
    $rootScope.$on(
      '$stateChangeStart', 
      function(event, toState, toParams, fromState, fromParams) {
        if (window.ga) {
          window.ga('send', 'pageview', { page: $location.url() });
        }
        if (firstLoad) {
          firstLoad = false;
          setTimeout(function(){ $('#js-app-preloader').fadeOut(250); }, 250);
        }
        $rootScope.highlight_page = !(~highlight_states.indexOf(toState.name));
      }
    );
  }
])
.config(['$stateProvider', '$urlRouterProvider', 
  function($stateProvider, $urlRouterProvider) {
    
    var regirect_routes = [
      '/users/logout',
      '/users/login/yandex',
      '/users/login/yandex-callback',
      '/users/login/google',
      '/users/login/google-callback',
      '/users/login/facebook',
      '/users/login/facebook-callback',
      '/users/login/vkontakte',
      '/users/login/vkontakte-callback'
    ];
    $urlRouterProvider.when('', '/');
    $urlRouterProvider.when('/users', '/users/login');
    $urlRouterProvider.rule(function ($injector, $location) {
      if (~regirect_routes.indexOf($location.path()))
        return window.location = $location.path();
    });

    $urlRouterProvider.otherwise('/404');

    $stateProvider
      .state('index', {
        url: '/',
        template: function(){ return $('#main-tpl').html() },
        controller: 'MainCtrl'
      })
      .state('challenge', {
        url: '/game/{date}',
        template: function(){ return $('#challenge-tpl').html() },
        controller: 'ChallengeCtrl'
      })
      .state('challenges', {
        // abstract: true,
        url: '/games',
        template: function(){ return $('#challenges-tpl').html() },
        controller: 'ChallengesCtrl'
      })
      // .state('challenges.list', {
      //   url: '/{year}.{month}',
      //   template: function(){ return $('#challenges-list-tpl').html() },
      //   controller: 'ChallengesListCtrl'
      // })

      .state('users', {
        abstract: true,
        url: '/users',
        template: function(){ return $('#users-tpl').html() },
        controller: 'UsersCtrl'
      })
      .state('users.login', {
        url: '/login',
        template: function(){ return $('#users-login-tpl').html() },
        controller: 'UsersLoginCtrl'
      })
      .state('users.register', {
        url: '/register',
        template: function(){ return $('#users-register-tpl').html() },
        controller: 'UsersRegisterCtrl'
      })
      .state('users.restore', {
        url: '/restore?token',
        template: function(){ return $('#users-restore-tpl').html() },
        controller: 'UsersRestoreCtrl'
      })
      .state('users.request_email', {
        url: '/request-email',
        template: function(){ return $('#users-request-email-tpl').html() },
        controller: 'UsersRequestEmailCtrl'
      })

      .state('profile', {
        abstract: true,
        url: '/profile',
        template: function(){ return $('#profile-tpl').html() },
        controller: 'ProfileCtrl'
      })
      .state('profile.awards', {
        url: '/awards',
        template: function(){ return $('#profile-awards-tpl').html() },
        controller: 'ProfileAwardsCtrl'
      })
      .state('profile.games', {
        url: '/games',
        template: function(){ return $('#profile-games-tpl').html() },
        controller: 'ProfileGamesCtrl'
      })
      .state('profile.settings', {
        url: '/settings',
        template: function(){ return $('#profile-settings-tpl').html() },
        controller: 'ProfileSettingsCtrl'
      })

      .state('rating', {
        url: '/rating',
        template: function(){ return $('#rating-tpl').html() },
        controller: 'RatingCtrl'
      })

      .state('rules', {
        url: '/rules',
        template: function(){ return $('#content-tpl').html() },
        controller: 'RulesCtrl'
      })

      .state('feedback', {
        url: '/feedback',
        template: function(){ return $('#feedback-tpl').html() },
        controller: 'FeedbackCtrl'
      })

      .state('404', {
        url: '/404',
        template: '404',
        controller:  function(){ 'Error404' }
      });
  }
]);