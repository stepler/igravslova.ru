'use strict';

var app_filters = angular.module('app_filters', []);

moment.locale('ru');
app_filters
.filter('moment', function() {
  return function(date, format) {
    return moment(parseInt(date)).format(format);
  };
})
.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});