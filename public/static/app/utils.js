'use strict';

var app_utilites = angular.module('app_utilites', []);

app_utilites
.factory('$app_forms', ['$location', 
  function($location) {
    return {
      submit_error: function(errors, callback) {
        var fields = [];
        return function(data){
          _.forEach(data, function(item) {
            fields.push(item.field);
            errors[item.field] = item.message || item.code;
          });
          for (var field in errors) {
            if (!~fields.indexOf(field)) {
              delete errors[field];
            }
          }
          if (typeof callback == 'function') {
            return callback.apply(this, arguments);
          }
        }
      },
      submit_success: function(callback) {
        return function(data) {
          if (_.has(data, 'redirect')) {
            return window.location = data.redirect;
          }
          if (_.has(data, 'location')) {
            return $location.path(data.location);
          }
          return callback.apply(this, arguments);
        }
      }
    };
  }
])
.factory('$system_modals', ['$modal', '$sce',
  function($modal, $sce) {
    return {
      success: function(data, close_callback) {
        $modal.open({
          template: (function(){ return $('#system-modal-success-tpl').html() })(),
          controller: function($scope, $modalInstance) {
            for (var key in data)
              $scope[key] = $sce.trustAsHtml(data[key]);
            
            if (close_callback)
              $scope.close = function() { close_callback.call($scope, $modalInstance) };
            else
              $scope.close = function () { $modalInstance.close('close'); };
          }
        });
      },
      error: function() {
        
      }
    };
  }
])
.factory('$page_type', ['$rootScope', 
  function($rootScope) {
    return {
      hightlight: function() {
        $rootScope.highlight_page = true;
      },
      normal: function() {
        $rootScope.highlight_page = false;
      }
    };
  }
])
.factory('challengeWordResize', [
  function() {
    return function(wordLength, isMainPage) {
      var max_cell_size = (60 / 100), // MAGIC DIGITS
          fond_size = 0,
          width = 0,
          timer = null;

      if (isMainPage) {
        max_cell_size = (75 / 100);
      }

      function calc() 
      {
        if (typeof(window.innerWidth) == 'number' ) {
          width = window.innerWidth;
        } 
        else if( document.documentElement && document.documentElement.clientWidth) {
          width = document.documentElement.clientWidth;
        }
        else {
          return; 
        }

        if (width > 991) {
          if (isMainPage) {
            width = 940;
            max_cell_size = (85 / 100);
          }
          else {
            width = 680;
            max_cell_size = (66 / 100);
          }
        }
        else if (width > 767) {
          width = 760;
        }
        else if (width > 480) {
          width *= 0.85;
        } 
        else {
          width = 410;
        }

        fond_size = Math.floor((width / max_cell_size) / wordLength);

        if (fond_size > 100) {
          fond_size = 100;
        }
        $('#js-style').text('.b-game {font-size:'+fond_size+'%}');
      }

      calc();

      $(window).off('resize');
      $(window).on('resize', function(){;
        clearTimeout(timer);
        timer = setTimeout(calc, 200);
      })
    }
  }
])
;