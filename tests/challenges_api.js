var config = global.config,
    schema = global.schema,
    path = require('path');

var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    tv4 = require('tv4');

var app_validator = require(path.join(config.path.middlewares, 'app_validator'))(),
    challenges = require(path.join(config.path.controllers, 'challenges'));

tv4.addFormat('date_object', function (data, schema) {
  return _.isDate(data);
});
tv4.addFormat('mongo_id', function (data, schema) {
  return null;
});

function emulate_express(handler, request, callback) {
  var req = {
        query: request.q || {},
        params: request.p || {},
        body: request.b || {},
        user: request.u || {},
        isAuthenticated: function(){ return !!request.u }
      },
      res = {
        json:callback
      };
  app_validator(req, res, function(){});
  handler(req, res);
}

module.exports = function() 
{
  return {

    // Получение списка челленджов
    api_list: function(test) 
    {
      // Check answer format
      emulate_express(challenges.list, {}, function(status, data) { 
        test.equal(status, 200);
        test.ok(tv4.validate(data, schema.challenge_list));
        test.equal(data.challenges.length, 6);

        // check limits
        emulate_express(challenges.list, { q:{ limit:2 } }, function(status, data) { 
          test.equal(status, 200);
          test.equal(data.challenges.length, 2);

          // check paging
          emulate_express(challenges.list, { q:{ limit:5, page:2 } }, function(status, data) { 
            test.equal(status, 200);
            test.equal(data.challenges.length, 1);

            // check year interval
            emulate_express(challenges.list, { q:{ year:2010 } }, function(status, data) { 
              test.equal(status, 200);
              test.equal(data.challenges.length, 3);

              // check month interval
              emulate_express(challenges.list, { q:{ year:2010, month:4 } }, function(status, data) { 
                test.equal(status, 200);
                test.equal(data.challenges.length, 2);

                // zero results on on unexist page
                emulate_express(challenges.list, { q:{ year:2010, month:5, limit:2, page:2 } }, function(status, data) { 
                  test.equal(status, 200);
                  test.equal(data.challenges.length, 0);

                  test.done();
                });
              });
            });
          });
        });
      });
    },

    api_get: function(test) 
    {
      // invalid date
      emulate_express(challenges.get, { p:{ date:'asdaaa' } }, function(status, data) {
        test.equal(status, 400);
        test.ok(tv4.validate(data, schema.api_error));

        // challenge for noauth user
        emulate_express(challenges.get, { p:{ date:'05.04.2011' } }, function(status, data) {
          test.equal(status, 200);
          test.ok(tv4.validate(data, schema.challenge_detail));
          test.equal(data.challenge.word, 'first_word');
          test.equal(data.answers.length, 0);

          // challenge for auth user
          emulate_express(challenges.get, 
            { p:{ date:'05.04.2011' }, u:{ _id:config.TEST_ID.u1 } }, function(status, data) {
            test.equal(status, 200);
            test.ok(tv4.validate(data, schema.challenge_detail));
            test.equal(data.answers.length, 9);

            test.done();
          });
        });
      });
    },

    api_add_answer: function(test)
    {
      // add answer for noauth user
      emulate_express(challenges.add_answer, { p:{ date:'05.04.2011' } }, function(status, data) {
        test.equal(status, 403);
        test.ok(tv4.validate(data, schema.api_error));

        // add answer with empty answer
        emulate_express(challenges.add_answer, 
          { p:{ date:'05.04.2011' }, u:{ _id:config.TEST_ID.u1 } }, function(status, data) {
          test.equal(status, 400);
          test.ok(tv4.validate(data, schema.api_error));

          // add invalid answer
          emulate_express(challenges.add_answer, 
          { p:{ date:'05.04.2011' }, b:{ answer:'invalid_answer' }, u:{ _id:config.TEST_ID.u1 } }, 
          function(status, data) {
            test.equal(status, 400);
            test.ok(tv4.validate(data, schema.api_error));
            
            // add valid answer
            emulate_express(challenges.add_answer, 
            { p:{ date:'05.04.2011' }, b:{ answer:'valid_answer' }, u:{ _id:config.TEST_ID.u1 } }, 
            function(status, data) {
              test.equal(status, 200);
              test.ok(tv4.validate(data, schema.challenge_add_answer));

              // check added word
              emulate_express(challenges.get, 
                { p:{ date:'05.04.2011' }, u:{ _id:config.TEST_ID.u1 } }, function(status, data) {
                test.equal(status, 200);
                test.ok(tv4.validate(data, schema.challenge_detail));
                test.ok(_.indexOf(data.answers, 'valid_answer') !== -1);

                test.done();
              });
            });
          });
        });
      });
    }
  }
};