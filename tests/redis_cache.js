var config = global.config,
    path = require('path');

var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose');

var Challenges = mongoose.model('Challenges'),
    Users = mongoose.model('Users'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    UsersRating = mongoose.model('UsersRating'),
    Dictionary = mongoose.model('Dictionary');

var storage = {},
    redis_cache = require(path.join(config.path.lib, 'redis_cache'));


module.exports = function() {
  return {
    setUp: function(callback) {
      redis_cache._redis_get = function(key, field, cb)
      {
        if (!storage[key]) {
          return cb(null, null);
        }
        if (!storage[key][field]) {
          return cb(null, null);
        }
        return cb(null, storage[key][field]);
      };
      redis_cache._redis_set = function(key, field, value, cb)
      {
        if (!storage[key]) {
          storage[key] = {};
        }
        storage[key][field] = value;
        return cb(null);
      };
      callback();
    },
    UsersRating: function(test) {
      var self = this,
          ID = config.TEST_ID;

      redis_cache.getUserRating(ID.u4, ['words_count', 'words_list'], 
        function(err, list) {
          test.equals(list.words_count, 3);
          test.equals(list.words_list.length, 3);
          test.done();
        }
      );
    },
    Challenges: function(test) {
      var self = this,
          ID = config.TEST_ID;
      redis_cache.getChallenge(ID.c1, ['word', 'is_active'], 
        function(err, list) {
          test.equals(list.word, 'first_word');
          test.equals(list.is_active, false);
          test.done();
        }
      );
    }
  }
};