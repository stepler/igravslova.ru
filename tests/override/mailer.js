
module.exports = {
  register: function(user) { },
  confirmEmailRequest: function(user) { },
  restoreRequest: function(user) { },
  restore: function(user) { },
  extend_dict: function(login, word, token) { },
  extend_dict_accept: function(mail_to, login, word) { },
  extend_dict_discart: function(mail_to, login, word) { },
  award_1: function(user, word, game_link) { },
  award_2: function(user, word, game_link) { },
  award_3: function(user, word, game_link) { },
  feedback: function(subject, email, message) { }
}