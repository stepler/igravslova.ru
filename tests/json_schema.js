exports.api_error = {
  "type": "array",
  "required": true,
  "items": {
    "type": "object",
    "required": false,
    "properties": {
      "field": {
        "type": "string",
        "required": true
      },
      "code": {
        "type": "string",
        "required": true
      },
      "message": {
        "type": "string",
        "required": true
      }
    }
  }
};

exports.challenge_list = {
  "type": "object",
  "required": true,
  "properties": {
    "challenges": {
      "type": "array",
      "required": true,
      "items": {
        "type": "object",
        "required": false,
        "properties": {
          "active_to": {
            "type": "object",
            "format": "date_object",
            "required": true
          },
          "answers_count": {
            "type": "integer",
            "required": true
          },
          "date": {
            "type": "object",
            "format": "date_object",
            "required": true
          },
          "is_active": {
            "type": "boolean",
            "required": true
          },
          "word": {
            "type": "string",
            "required": true
          }
        }
      }
    }
  }
};

exports.challenge_detail = {
  "type": "object",
  "required": true,
  "properties": {
    "challenge": {
      "type": "object",
      "required": true,
      "properties": {
        "active_to": {
          "type": "object",
          "format": "date_object",
          "required": true
        },
        "answers_count": {
          "type": "integer",
          "required": true
        },
        "date": {
          "type": "object",
          "format": "date_object",
          "required": true
        },
        "is_active": {
          "type": "boolean",
          "required": true
        },
        "word": {
          "type": "string",
          "required": true
        }
      }
    },
    "answers": {
      "type": "array",
      "required": false,
      "items": { "type": "string" }
    },
    "rating": {
      "type": "array",
      "required": false,
      "items": {
        "type": "object",
        "properties": {
          
          "challenge": {
            "type": "object",
            "format": "mongo_id",
            "required": true
          },
          "answers_count": {
            "type": "integer",
            "required": true
          },
          "position": {
            "type": "integer",
            "required": true
          },
          "user": {
            "type": "object",
            "required": true,
            "properties": {
              
              "login": {
                "type": "string",
                "required": true
              },
              "avatar": {
                "type": "string",
                "required": true
              }
            }
          }
        }
      }
    }
  }
};

exports.challenge_with_results = {
  "type": "object",
  "required": true,
  "properties": {
    "active_to": {
      "type": "object",
      "format": "date_object",
      "required": true
    },
    "answers_count": {
      "type": "integer",
      "required": true
    },
    "date": {
      "type": "object",
      "format": "date_object",
      "required": true
    },
    "is_active": {
      "type": "boolean",
      "required": true
    },
    "word": {
      "type": "string",
      "required": true
    },
    "results": {
      "type": "array",
      "required": true,
      "items": {
        "type": "object",
        "properties": {
          "challenge": {
            "type": "object",
            "format": "mongo_id",
            "required": true
          },
          "answers_count": {
            "type": "integer",
            "required": true
          },
          "position": {
            "type": "integer",
            "required": true
          },
          "user": {
            "type": "object",
            "required": true,
            "properties": {
              
              "login": {
                "type": "string",
                "required": true
              },
              "avatar": {
                "type": "string",
                "required": true
              }
            }
          }
        }
      }
    }
  }
};

exports.challenge_add_answer = {
  "type": "object",
  "required": true,
  "properties": {
    "rating": {
      "type": "array",
      "required": true,
      "items": {
        "type": "object",
        "properties": {
          "challenge": {
            "type": "object",
            "format": "mongo_id",
            "required": true
          },
          "answers_count": {
            "type": "integer",
            "required": true
          },
          "position": {
            "type": "integer",
            "required": true
          },
          "user": {
            "type": "object",
            "required": true,
            "properties": {
              
              "login": {
                "type": "string",
                "required": true
              },
              "avatar": {
                "type": "string",
                "required": true
              }
            }
          }
        }
      }
    }
  }
};