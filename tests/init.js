var config = global.config,
    path = require('path');

var async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose');

var Challenges = mongoose.model('Challenges'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    Users = mongoose.model('Users'),
    UsersRating = mongoose.model('UsersRating'),
    Dictionary = mongoose.model('Dictionary');

module.exports = function() {
  return {
    setUp: function (callback) 
    {
      var ID = {
        c1: mongoose.Types.ObjectId(),
        c2: mongoose.Types.ObjectId(),
        c3: mongoose.Types.ObjectId(),
        u1: mongoose.Types.ObjectId(),
        u2: mongoose.Types.ObjectId(),
        u3: mongoose.Types.ObjectId(),
        u4: mongoose.Types.ObjectId(),
      };
      config.TEST_ID = ID;

      async.series([
        // clear UserWords in dictionary
        function (callback) {
          // Dictionary.remove({ type:'user-word' }, callback);
          callback(null);
        },
        function(callback) {
          if (!mongoose.connection.collections['challenges'])
            return callback(null);
          mongoose.connection.collections['challenges'].drop( function(err) {
            callback(null);
          });
        },
        function(callback) {
          if (!mongoose.connection.collections['users'])
            return callback(null);
          mongoose.connection.collections['users'].drop( function(err) {
            callback(null);
          });
        },
        function(callback) {
          if (!mongoose.connection.collections['challenges_answers'])
            return callback(null);
          mongoose.connection.collections['challenges_answers'].drop( function(err) {
            callback(null);
          });
        },
        function(callback) {
          if (!mongoose.connection.collections['users_rating'])
            return callback(null);
          mongoose.connection.collections['users_rating'].drop( function(err) {
            callback(null);
          });
        },
        function(callback) {
          if (!mongoose.connection.collections['dictionary_userword'])
            return callback(null);
          mongoose.connection.collections['dictionary_userword'].drop( function(err) {
            callback(null);
          });
        },
        function(callback) {
          if (!mongoose.connection.collections['dictionary'])
            return callback(null);
          mongoose.connection.collections['dictionary'].drop( function(err) {
            callback(null);
          });
        },
        // setChallenges
        function (callback) {
          var pastYear = moment({ year:2010, month:3, day:5, hour:0, minute:0, second:0, millisecond:0 }),
              dateStart = moment({year:2011, month:3, day:5, hour: 0, minute: 0, second: 0, millisecond: 0}),
              dateEnd = dateStart.clone().add('days', 10);

          Challenges.create([
            {
              _id: ID.c1,
              word: 'first_word',
              date: dateStart.toDate(),
              active_to: dateEnd.toDate(),
              answers_list: ['valid_answer'],
              is_active: true,
              rating: []
            },{
              _id: ID.c2,
              word: 'second_word',
              date: dateStart.clone().add('days', 1).toDate(),
              active_to: dateEnd.clone().add('days', 1).toDate(),
              answers_list: [],
              is_active: true,
              rating: []
            },{
              _id: ID.c3,
              word: 'third_word',
              date: dateStart.clone().add('days', 2).toDate(),
              active_to: dateEnd.clone().add('days', 2).toDate(),
              answers_list: [],
              is_active: true,
              rating: []
            },{
              _id: mongoose.Types.ObjectId(),
              word: 'past_word_1',
              date: pastYear.toDate(),
              active_to: pastYear.clone().add('days', 30).toDate(),
              answers_list: [],
              is_active: false,
              rating: []
            },{
              _id: mongoose.Types.ObjectId(),
              word: 'past_word_2',
              date:      pastYear.clone().add('days', 1).toDate(),
              active_to: pastYear.clone().add('days', 31).toDate(),
              answers_list: [],
              is_active: false,
              rating: []
            },{
              _id: mongoose.Types.ObjectId(),
              word: 'past_word_3',
              date:      pastYear.clone().add('months', 1).toDate(),
              active_to: pastYear.clone().add('months', 1).add('days', 30).toDate(),
              answers_list: [],
              is_active: false,
              rating: []
            },
          ], 
          callback);
        },
        // setUsers
        function (callback) {
          Users.create([
            {
              _id: ID.u1,
              provider: 'local',
              login: 'user1',
              email: 'mail@user1',
              avatar: 'ava-user1',
              password: 'pass_user1',
              salt: 'salt_user1',
              restore_token: null,
              profile: {}
            },{
              _id: ID.u2,
              provider: 'local',
              login: 'user2',
              email: 'mail@user2',
              avatar: 'ava-user2',
              password: 'pass_user2',
              salt: 'salt_user2',
              restore_token: null,
              profile: {}
            },{
              _id: ID.u3,
              provider: 'local',
              login: 'user3',
              email: 'mail@user3',
              avatar: 'ava-user3',
              password: 'pass_user3',
              salt: 'salt_user3',
              restore_token: null,
              profile: {}
            },{
              _id: ID.u4,
              provider: 'local',
              login: 'user4',
              email: 'mail@user4',
              avatar: 'ava-user4',
              password: 'pass_user4',
              salt: 'salt_user4',
              restore_token: null,
              profile: {}
            }
          ], 
          callback);
        },
        // setUsersRatings
        function (callback) {
          UsersRating.create([
            {
              
            }
          ],
          callback);
        },
        // setChallengesAnswers
        function (callback) {
          var date = moment({hour: 0, minute: 0, second: 0, millisecond: 0});
          ChallengesAnswers.create([
            {
              challenge: ID.c1,
              user: ID.u1,
              sort: date.toDate().getTime(),
              answers_list: ['c1u1a1','c1u1a2','c1u1a3','c1u1a4','c1u1a5','c1u1a6','c1u1a7','c1u1a8','c1u1a9'],
              answers_count: 9
            },{
              challenge: ID.c1,
              user: ID.u2,
              sort: date.toDate().getTime(),
              answers_list: ['c1u2a1','c1u2a2','c1u2a3','c1u2a4','c1u2a5','c1u2a6','c1u2a7','c1u2a8','c1u2a9','c1u2a10'],
              answers_count: 10
            },{
              challenge: ID.c1,
              user: ID.u3,
              sort: date.toDate().getTime(),
              answers_list: ['c1u3a1','c1u3a2','c1u3a3','c1u3a4','c1u3a5'],
              answers_count: 5
            },{
              challenge: ID.c1,
              user: ID.u4,
              sort: date.toDate().getTime(),
              answers_list: ['c1u4a1'],
              answers_count: 1
            },{
              challenge: ID.c2,
              user: ID.u1,
              sort: date.toDate().getTime(),
              answers_list: ['c2u1a1'],
              answers_count: 1
            },{
              challenge: ID.c2,
              user: ID.u2,
              sort: date.toDate().getTime(),
              answers_list: ['c2u2a1','c2u2a2','c2u2a3','c2u2a4','c2u2a5'],
              answers_count: 5
            },{
              challenge: ID.c2,
              user: ID.u3,
              sort: date.toDate().getTime(),
              answers_list: ['c2u3a1','c2u3a2','c2u3a3','c2u3a4','c2u3a5','c2u3a6','c2u3a7','c2u3a8','c2u3a9','c2u3a10','c2u3a11','c2u3a12','c2u3a13','c2u3a14','c2u3a15'],
              answers_count: 15
            },{
              challenge: ID.c2,
              user: ID.u4,
              sort: date.toDate().getTime(),
              answers_list: ['c2u4a1','c2u4a2','c2u4a3','c2u4a4','c2u4a5','c2u4a6','c2u4a7','c2u4a8','c2u4a9','c2u4a10'],
              answers_count: 10
            }
          ], 
          callback);
        },
        function (callback) {
          Dictionary.create([
            {
              word: 'next_word',
              index: ('next_word'.split('').sort().join('')),
              length: ('next_word'.length),
              type: '',
              type_raw: ''
            },{
              word: 'second_dict_word',
              index: ('second_dict_word'.split('').sort().join('')),
              length: ('second_dict_word'.length),
              type: '',
              type_raw: ''
            },{
              word: 'third_dict_word',
              index: ('third_dict_word'.split('').sort().join('')),
              length: ('third_dict_word'.length),
              type: '',
              type_raw: ''
            }
          ],
          callback);
        }
      ],
      function(err, results){
        callback();
      });
    },
    tearDown: function (callback) {
      callback();
    },
    init: function (test) {
      test.done();
    }
  }
};