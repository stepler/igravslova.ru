var config = global.config,
    schema = global.schema,
    path = require('path');

var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose'),
    tv4 = require('tv4');

var Challenges = mongoose.model('Challenges'),
    Users = mongoose.model('Users'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    UsersRating = mongoose.model('UsersRating'),
    Dictionary = mongoose.model('Dictionary');

var tasks = require(path.join(config.path.controllers, 'tasks'));

tv4.addFormat('date_object', function (data, schema) {
  return _.isDate(data);
});
tv4.addFormat('mongo_id', function (data, schema) {
  return null;
});

module.exports = function() {
  return {

    complete_challenge: function (test) 
    {
      var ID = config.TEST_ID,
          counter = 0,
          today = moment({hour: 0, minute: 0, second: 0, millisecond: 0});

      Challenges.count({ 'active_to':{ $lte:today.toDate() }, 
        'is_active':true }).lean().exec(function(err, count) {

        tasks.completeChallenge(function(err, success) {
          test.ok(_.isNull(err));
          test.ok(_.isString(success));
          if (++counter == count) {
            check_challenges();
          }
        });
      });

      function check_challenges()
      {
        Challenges.findOne({ word:'first_word' })
        .lean().exec(function(err, challenge) {
          test.ok(tv4.validate(challenge, schema.challenge_with_results));
          test.equal(challenge.is_active, false);
          test.equal(challenge.results.length, 4);
          test.equal(challenge.results[0].user._id, ID.u1.toString());
          test.equal(challenge.results[0].position, 1);
          test.equal(challenge.results[1].user._id, ID.u2.toString());
          test.equal(challenge.results[1].position, 1);
          test.equal(challenge.results[2].user._id, ID.u3.toString());
          test.equal(challenge.results[2].position, 2);
          test.equal(challenge.results[3].user._id, ID.u4.toString());
          test.equal(challenge.results[3].position, 3);

          Challenges.findOne({ word:'second_word' })
          .lean().exec(function(err, challenge) {
            test.ok(tv4.validate(challenge, schema.challenge_with_results));
            test.equal(challenge.is_active, false);
            test.equal(challenge.results.length, 4);
            test.equal(challenge.results[0].user._id, ID.u3.toString());
            test.equal(challenge.results[0].position, 1);
            test.equal(challenge.results[1].user._id, ID.u4.toString());
            test.equal(challenge.results[1].position, 2);
            test.equal(challenge.results[2].user._id, ID.u2.toString());
            test.equal(challenge.results[2].position, 3);
            test.equal(challenge.results[3].user._id, ID.u1.toString());
            test.equal(challenge.results[3].position, 4);

            Challenges.findOne({ word:'third_word' })
            .lean().exec(function(err, challenge) {
              test.equal(challenge.is_active, false);
              test.equal(challenge.results.length, 0);

              check_user_rating();
            });
          });
        });
      }

      function check_user_rating()
      {

        UsersRating.findOne({ user:ID.u1} ).lean().exec(function(err, user){
          test.equal(user.rating, 100);
          test.equal(user.awards.g, 1);
          test.equal(user.awards_list.g[0][1], 'first_word');
          test.equal(user.awards_list.g[0][2], 100);

          UsersRating.findOne({ user:ID.u2} ).lean().exec(function(err, user){
            test.equal(user.rating, 140);
            test.equal(user.awards.g, 1);
            test.equal(user.awards_list.g[0][1], 'first_word');
            test.equal(user.awards_list.g[0][2], 100);
            test.equal(user.awards.b, 1);
            test.equal(user.awards_list.b[0][1], 'second_word');
            test.equal(user.awards_list.b[0][2], 40);

            UsersRating.findOne({ user:ID.u3} ).lean().exec(function(err, user){
              test.equal(user.rating, 160);
              test.equal(user.awards.g, 1);
              test.equal(user.awards_list.g[0][1], 'second_word');
              test.equal(user.awards_list.g[0][2], 100);
              test.equal(user.awards.s, 1);
              test.equal(user.awards_list.s[0][1], 'first_word');
              test.equal(user.awards_list.s[0][2], 60);

              test.done();
            });
          });
        });
      }
    },

    find_next_challenge: function(test)
    {
      tasks.findNextChallenge(function(err, success) {
        test.ok(_.isNull(err));
        test.ok(_.isString(success));
        
        Dictionary.findOne({ word:'next_word' })
        .lean().exec(function(err, word) {
          test.ok(word.is_next);
          test.ok(!word.is_use);

          test.done();
        })
      });
    },

    add_new_challenge: function(test)
    {
      tasks.addChallenge(function(err, success) {
        test.ok(_.isNull(err));
        test.ok(_.isString(success));
        
        Dictionary.findOne({ word:'next_word' })
        .lean().exec(function(err, word) {
          test.ok(!word.is_next);
          test.ok(word.is_use);

          Challenges.findOne({ word:'next_word' })
          .lean().exec(function(err, challenge) {
            var date = moment(config.challenges.active_time),
                active_to = moment(config.challenges.active_time).add('days', config.challenges.active_days);

            test.equal(challenge.is_active, true);
            test.equal(challenge.date.getTime(), date.toDate().getTime());
            test.equal(challenge.active_to.getTime(), active_to.toDate().getTime());

            test.done();
          });
        })
      });
    }
/*
    checkAchievements: function (test) {
      test.done();
      return;
      var self = this,
          ID = config.TEST_ID;

      async.series([
        function (callback) {
          tasksCtrl.checkAchievements(ID.u1, 'words', function(err, added_list) {
            test.equal(err, null);
            test.ok(added_list);
            callback(null);
          });
        }
      ],
      function(err, results){
        test.done();
      });
    },

    completeChallenge: function (test) {
      test.done();
      return;
      var self = this,
          ID = config.TEST_ID,
          today = moment({hour: 0, minute: 0, second: 0, millisecond: 0});

      // test.done();
      tasksCtrl.completeChallenge(function(){
        async.parallel([
          function (callback) {
            UsersRating.findOne({user:ID.u1}, function(err, item){
              test.equals(item.rating, config.challenges.score[1]);
              test.deepEqual(item.awards, { g:1, s:0, b:0, t10:0 });
              test.deepEqual(item.awards_list, 
                { g:[[today.toDate(), 'first_word', config.challenges.score[1]]], s:[], b:[], t10:[] });
              callback(null);
            });
          },
          function (callback) {
            UsersRating.findOne({user:ID.u2}, function(err, item){
              test.equals(item.rating, config.challenges.score[1]);
              test.deepEqual(item.awards, { g:1, s:0, b:0, t10:0 });
              test.deepEqual(item.awards_list, 
                { g:[[today.toDate(), 'first_word', config.challenges.score[1]]], s:[], b:[], t10:[] });
              callback(null);
            });
          },
          function (callback) {
            UsersRating.findOne({user:ID.u3}, function(err, item){
              test.equals(item.rating, config.challenges.score[2]);
              test.deepEqual(item.awards, { g:0, s:1, b:0, t10:0 });
              test.deepEqual(item.awards_list, 
                { g:[], s:[[today.toDate(), 'first_word', config.challenges.score[2]]], b:[], t10:[] });
              callback(null);
            });
          },
          function (callback) {
            UsersRating.findOne({user:ID.u4}, function(err, item){
              test.equals(item.rating, config.challenges.score[3]);
              test.deepEqual(item.awards, { g:0, s:0, b:1, t10:0 });
              test.deepEqual(item.awards_list, 
                { g:[], s:[], b:[[today.toDate(), 'first_word', config.challenges.score[3]]], t10:[] });
              callback(null);
            });
          },
          function (callback) {
            Challenges.findOne({ active_to:today.toDate() }, function(err, challenge) {
              test.ok(!challenge.is_active);
              callback(null);
            });
          }
        ],
        function(err, results){
          test.done();
        });
      });
    },
    extendDict: function (test) {
      test.done();
      return;
      var self = this,
          NEW_WORD_1 = 'wd',
          NEW_WORD_2 = 'fwd', // only ID.c1
          NEW_WORD_3 = 'ewd', // only ID.c2
          ID = config.TEST_ID;

      // test.done();
      async.series([
        function (callback) {
          tasksCtrl.extendDict(ID.u1, ID.c1, NEW_WORD_1, function(err, results) {
            test.ok(results.dictionary === true);
            test.ok(results.user_answer === true);
            test.ok(results.challenges === true);
            async.parallel([
              function (callback) {
                Dictionary.findOne({ word:NEW_WORD_1 }, function(err, word) {
                  test.ok(word !== null);
                  callback(null);
                });
              },
              function (callback) {
                Challenges.findById(ID.c1, function(err, challenge) {
                  test.ok(~challenge.answers_list.indexOf(NEW_WORD_1));
                  callback(null);
                });
              },
              function (callback) {
                Challenges.findById(ID.c2, function(err, challenge) {
                  test.ok(~challenge.answers_list.indexOf(NEW_WORD_1));
                  callback(null);
                });
              }
            ],
            function(err, results){
              callback(null);
            });
          });
        },
        function (callback) {
          tasksCtrl.extendDict(ID.u1, ID.c1, NEW_WORD_2, function(err, results) {
            test.ok(results.dictionary === true);
            test.ok(results.user_answer === true);
            test.ok(results.challenges === true);
            Challenges.findById(ID.c2, function(err, challenge) {
              test.ok(!~challenge.answers_list.indexOf(NEW_WORD_2));
              callback(null);
            });
          });
        },
        function (callback) {
          tasksCtrl.extendDict(ID.u1, ID.c1, NEW_WORD_3, function(err, results) {
            test.ok(results.dictionary === true);
            test.ok(results.user_answer === false);
            test.ok(results.challenges === true);
            Challenges.findById(ID.c1, function(err, challenge) {
              test.ok(!~challenge.answers_list.indexOf(NEW_WORD_3));
              callback(null);
            });
          });
        },
        function (callback) {
          tasksCtrl.extendDict(ID.u1, ID.c1, NEW_WORD_1, function(err, results) {
            test.ok(results.dictionary === false);
            test.ok(results.user_answer === undefined);
            test.ok(results.challenges === undefined);
            callback(null);
          });
        }
      ],
      function () {
        test.done();
      });
    }
*/
  }
};