var config = global.config,
    path = require('path');

var _ = require('lodash'),
    async = require('async'),
    moment = require('moment'),
    mongoose = require('mongoose');

var Challenges = mongoose.model('Challenges'),
    Users = mongoose.model('Users'),
    ChallengesAnswers = mongoose.model('ChallengesAnswers'),
    UsersRating = mongoose.model('UsersRating'),
    Dictionary = mongoose.model('Dictionary');

var share = {};

module.exports = function() {
  return {
    setUp: function(callback) {
      config.challenges.words_length = [3,4];
      callback();
    },
    AddChallengeNoExist: function(test) {
      Dictionary.add_challenge(function(err, word) {
        test.equal(err, null);
        test.equal(word, null);
        test.done();
      });
    },
    FindNewChallenge: function(test) {
      Dictionary.find_next_challenge(function(err, word) {
        test.equal(err, null);
        test.ok(word.is_next);
        test.ok(_.isArray(word.answers_list));
        test.ok(word.answers_list.length > 0);

        share.word_to_challenge = word;
        test.done();
      });
    },
    AddChallenge: function(test) {
      Dictionary.add_challenge(function(err, challenge) {
        test.equal(err, null);
        test.equal(challenge.word, share.word_to_challenge.word);
        test.deepEqual(challenge.answers_list.length, share.word_to_challenge.answers_list.length);
        test.done();
      });
    }
  }
};