var config = global.config,
    path = require('path');

var _ = require('lodash'),
    async = require('async');

var achievements = require(path.join(config.path.conf, 'achievements'));

var share = {};

module.exports = function() {
  return {
    AchievementsWords: function(test) {
      achievements.words.get(1, [], function(added, all) {
        test.deepEqual(added, ['w1'])
      });
      achievements.words.get(1000, [], function(added, all) {
        test.deepEqual(added, ['w1', 'w100', 'w500', 'w1000'])
      });
      achievements.words.get(1000, ['w1'], function(added, all) {
        test.deepEqual(added, ['w100', 'w500', 'w1000'])
        test.deepEqual(all, ['w1', 'w100', 'w500', 'w1000'])
      });
      achievements.words.get(1000, ['w1', 'w100', 'w500'], function(added, all) {
        test.deepEqual(added, ['w1000'])
        test.deepEqual(all, ['w1', 'w100', 'w500', 'w1000'])
      });
      achievements.words.get(600, ['w1', 'w100', 'w500'], function(added, all) {
        test.deepEqual(added, [])
        test.deepEqual(all, ['w1', 'w100', 'w500'])
      });
      test.done();
    }
  }
};